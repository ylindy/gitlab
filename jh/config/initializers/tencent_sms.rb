# frozen_string_literal: true

require 'tencentcloud-sdk-common'
require 'tencentcloud-sdk-sms'

module TencentSms
  include TencentCloud::Common
  include TencentCloud::Sms::V20210111

  PhoneNumberError = Class.new(StandardError)

  def self.send_code(phone)
    validate_phone_number(phone)

    cre = Credential.new(ENV['TC_ID'], ENV['TC_KEY'])
    cli = Client.new(cre, 'ap-beijing')
    random_code = (SecureRandom.random_number(9e5) + 1e5).to_i.to_s

    phonenumberset = ["#{phone}"]
    smssdkappid = ENV['TC_APP_ID'] || "1400551828"
    templateid = ENV['TC_TEMPLATE_ID'] || "1176342"
    signname = ENV['TC_SIGN_NAME'] || "极狐GitLab"
    templateparamset = [random_code, "10"]
    extendcode = nil
    sessioncontext = nil
    senderid = nil

    req = SendSmsRequest.new(
      phonenumberset,
      smssdkappid,
      templateid,
      signname,
      templateparamset,
      extendcode,
      sessioncontext,
      senderid
    )
    cli.SendSms(req)
    random_code
  rescue TencentCloudSDKException => e
    Gitlab::AppLogger.error(e)
    nil
  rescue PhoneNumberError => e
    Gitlab::AppLogger.error(e)
    nil
  end

  def self.validate_phone_number(phone)
    raise PhoneNumberError, "Invalid Phone Number" unless phone =~ /^\+\d+$/

    len = phone.length
    if phone.start_with?("+86")
      raise PhoneNumberError, "Invalid China Mainland Phone Number" if len != 14
    elsif phone.start_with?("+852")
      raise PhoneNumberError, "Invalid China HongKong Phone Number" if len > 12 || len < 11
    elsif phone.start_with?("+853")
      raise PhoneNumberError, "Invalid China Macao Phone Number" if len > 12 || len < 11
    else
      raise PhoneNumberError, "Unsupported Region Phone Number"
    end
  end
end

# frozen_string_literal: true

module JH
  # ApplicationSetting JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the `ApplicationSetting` model
  module ApplicationSetting
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      validates :content_validation_endpoint_url,
        addressable_url: true, allow_blank: true

      validates :content_validation_endpoint_url,
        presence: true,
        if: :content_validation_endpoint_enabled

      validates :content_validation_api_key,
        length: { maximum: 2000, message: _('is too long (maximum is %{count} characters)') },
        allow_blank: true

      validates :content_validation_api_key,
        presence: true,
        if: :content_validation_endpoint_enabled

      attr_encrypted :content_validation_api_key, encryption_options_base_32_aes_256_gcm.merge(encode: false)
    end

    class_methods do
      extend ::Gitlab::Utils::Override

      override :defaults
      def defaults
        super.merge(
          content_validation_enabled: false,
          content_validation_endpoint_url: nil,
          content_validation_api_key: nil
        )
      end
    end
  end
end

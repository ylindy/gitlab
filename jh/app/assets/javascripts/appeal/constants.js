import { s__ } from '~/locale';

export const APPEAL_MODAL_ID = 'appeal';
export const ERROR_MESSAGE = s__('ContentValidation|An error occurred while submitting appeal');
export const SUCCESS_MESSAGE = s__('ContentValidation|Appeal submitted');

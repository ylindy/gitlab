/* eslint-disable import/export */
export * from '~/lib/utils/url_utility';

// About JiHu(GitLab) default host
export const PROMO_HOST = 'about.gitlab.cn';

// About JiHu(GitLab) default url
export const PROMO_URL = `https://${PROMO_HOST}`;

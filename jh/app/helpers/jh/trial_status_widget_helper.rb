# frozen_string_literal: true

module JH
  module TrialStatusWidgetHelper
    include ::Gitlab::Utils::StrongMemoize
    extend ::Gitlab::Utils::Override

    private

    override :ultimate_subscription_path_for_group
    def ultimate_subscription_path_for_group(group)
      ultimate_plan_purchase_link(group)
    end

    def ultimate_plan_purchase_link(group)
      href = ultimate_plan_data&.dig('purchase_link', 'href')
      uri = URI.parse(href)
      uri.query = [uri.query, "gl_namespace_id=#{group.id}"].compact.join('&')
      uri.to_s
    rescue URI::InvalidURIError => _e
      ::Gitlab::SubscriptionPortal.subscriptions_url
    end

    def ultimate_plan_data
      strong_memoize(:ultimate_plan_data) do
        plans = GitlabSubscriptions::FetchSubscriptionPlansService.new(plan: :free).execute
        next unless plans

        plans.find { |data| data['code'] == 'ultimate' }
      end
    end
  end
end

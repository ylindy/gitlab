# frozen_string_literal: true

module JH
  module ApplicationHelper
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    class_methods do
      extend ::Gitlab::Utils::Override

      override :promo_host
      def promo_host
        'about.gitlab.cn'
      end
    end

    def render_ee(partial, locals = {})
      render template: find_ee_template(partial), locals: locals
    end

    # rubocop: disable CodeReuse/ActiveRecord
    def find_ee_template(name)
      prefixes = [] # So don't create extra [] garbage

      if ee_lookup_context.exists?(name, prefixes, true)
        ee_lookup_context.find(name, prefixes, true)
      else
        ee_lookup_context.find(name, prefixes, false)
      end
    end
    # rubocop: enable CodeReuse/ActiveRecord

    def ee_lookup_context
      @ee_lookup_context ||= fetch_lookup_context(::Gitlab.extensions.last)
    end

    override :ce_lookup_context
    def ce_lookup_context
      @ce_lookup_context ||= fetch_lookup_context(::Gitlab.extensions.reverse)
    end

    private

    def fetch_lookup_context(folders)
      raise 'folders should not be blank' if folders.blank?

      folder_paths = Array(folders).map { |folder| "#{Rails.root}/#{folder}" }
      view_paths = lookup_context.view_paths.paths.reject do |resolver|
        resolver.to_path.start_with?(*folder_paths)
      end

      ActionView::LookupContext.new(view_paths)
    end
  end
end

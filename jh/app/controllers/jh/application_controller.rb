# frozen_string_literal: true

module JH
  module ApplicationController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    # JH add one more step in enforce_terms! for phone verification
    override :enforce_terms!
    def enforce_terms!
      super

      if ::Feature.enabled?(:real_name_system)
        enforce_phone_verification! if current_user
      end
    end

    override :can?
    def can?(object, action, subject = :global)
      # Monkey patch the method of `can?` only in the page of terms index
      if controller_name == "terms" && action_name == "index" && ::Feature.enabled?(:real_name_system)
        !object.phone_verified? || super(object, action, subject) if action == :accept_terms || action == :decline_terms
      else
        super(object, action, subject)
      end
    end

    override :require_email
    def require_email
      return if action_name == "phone_exists"

      super
    end

    private

    def enforce_phone_verification!
      return if current_user.phone_verified?

      redirect_path = if request.get?
                        request.fullpath
                      else
                        URI(request.referer).path if request.referer
                      end

      if redirect_path != '/users/sign_up/welcome'
        flash[:notice] = s_("RealName|Please verify your phone before continuing.")
        redirect_to terms_path(redirect: redirect_path), status: :found unless performed?
      end
    end
  end
end

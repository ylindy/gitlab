# frozen_string_literal: true

module JH
  module Gitlab
    module SubscriptionPortal
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :default_subscriptions_url
        def default_subscriptions_url
          ::Gitlab.dev_or_test_env? ? 'https://customers.stg.jihulab.com' : 'https://customers.jihulab.com'
        end

        override :subscriptions_comparison_url
        def subscriptions_comparison_url
          "https://about.gitlab.cn/pricing/saas/feature-comparison"
        end
      end
    end
  end
end

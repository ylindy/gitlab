# frozen_string_literal: true

module JH
  module Gitlab
    module Template
      module GitlabCiYmlTemplate
        extend ActiveSupport::Concern

        class_methods do
          extend ::Gitlab::Utils::Override

          override :base_dir
          def base_dir
            Rails.root.join('jh/lib/gitlab/ci/templates')
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

RSpec.describe TencentSms do
  let(:phone) { '15688886666' }

  describe '.send_code' do
    it 'returns nil if TC_ID TC_KEY not set' do
      expect(described_class.send_code(phone)).to be_falsey
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

# rubocop: disable CodeReuse/ActiveRecord
RSpec.describe Gitlab::ContentValidation::Client do
  let(:endpoint) { 'https://content_validation.url' }
  let(:client) { Gitlab::ContentValidation::Client.new }

  describe '#valid?' do
    let(:url) { "#{endpoint}/api/content_validation/validate" }

    context 'with invalid content' do
      before do
        stub_content_validation_request(false)
      end

      it "with sensitive word" do
        expect(client.valid?("sensitive")).to be false
      end
    end

    context 'with valid content' do
      before do
        stub_content_validation_request(true)
      end
      it 'with nosensitive word' do
        expect(client.valid?("gitlab")).to be true
      end
    end

    context 'client request timeout' do
      it do
        WebMock.stub_request(:post, url).to_timeout
        expect(client.valid?("gitlab")).to be true
      end
    end

    context 'client request raise error' do
      it do
        WebMock.stub_request(:post, url).to_raise(StandardError)
        expect(client.valid?("gitlab")).to be true
      end
    end

    context 'client disalbed' do
      it do
        WebMock.disable_net_connect!
        expect(client.valid?("gitlab")).to be true
        WebMock.enable_net_connect!
      end
    end
  end
end
# rubocop: enable CodeReuse/ActiveRecord

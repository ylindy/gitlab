export const phones = {
  mainland: {
    phone: '13888888888',
    areaCode: '+86',
  },
  hkSAR: {
    phone: '88888888',
    areaCode: '+852',
  },
  macauSAR: {
    phone: '88888888',
    areaCode: '+853',
  },
};

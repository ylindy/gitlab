import path from 'path';

const ROOT_PATH = path.resolve(__dirname, '../../../../');

const injectTestFileMap = {
  'spec/frontend/repository/components/tree_content_spec.js': async () => {
    const appealApi = await import('jh/api/appeal_api');
    jest
      .spyOn(appealApi, 'getTreeContentBlockedState')
      .mockReturnValue(Promise.resolve({ data: null }));
  },
};

beforeEach(() => {
  const testPath = path.relative(ROOT_PATH, jasmine.testPath);

  if (typeof injectTestFileMap[testPath] !== 'undefined') {
    injectTestFileMap[testPath]();
  }
});

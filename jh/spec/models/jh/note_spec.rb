# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Note do
  describe 'validations' do
    it_behaves_like "content validation with project", :note, :note
  end
end

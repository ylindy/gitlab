# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ApplicationSetting do
  using RSpec::Parameterized::TableSyntax

  subject(:setting) { described_class.create_from_defaults }

  describe 'validations' do
    describe 'content_validation_endpoint' do
      context 'when content_validation_endpoint is enabled' do
        before do
          setting.content_validation_endpoint_enabled = true
        end
        it { is_expected.to allow_value('https://example.org/content_validation').for(:content_validation_endpoint_url) }
        it { is_expected.not_to allow_value('nonsense').for(:content_validation_endpoint_url) }
        it { is_expected.not_to allow_value(nil).for(:content_validation_endpoint_url) }
        it { is_expected.not_to allow_value('').for(:content_validation_endpoint_url) }
      end

      context 'when content_validation_endpoint is NOT enabled' do
        before do
          setting.content_validation_endpoint_enabled = false
        end
        it { is_expected.not_to allow_value('nonsense').for(:content_validation_endpoint_url) }
        it { is_expected.to allow_value(nil).for(:content_validation_endpoint_url) }
        it { is_expected.to allow_value('').for(:content_validation_endpoint_url) }
      end
    end
  end
end

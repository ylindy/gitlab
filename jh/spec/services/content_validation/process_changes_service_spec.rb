# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ContentValidation::ProcessChangesService do
  let_it_be(:group)    { create(:group) }
  let_it_be(:user)     { create(:user) }
  let_it_be(:project)  { create(:project, :repository, :public, group: group) }

  let(:container) { project }
  let(:repo_type) { Gitlab::GlRepository::PROJECT }
  let(:default_branch) { container.default_branch }
  let(:commit) { container.repository.commit(default_branch) }
  let(:changes) { [{ oldrev: Gitlab::Git::BLANK_SHA, newrev: commit.id, ref: "refs/heads/#{default_branch}" }] }

  before do
    allow(ContentValidation::Setting).to receive(:check_enabled?).and_return(true)
  end

  subject { described_class.new(container: container, project: project, repo_type: repo_type, user: user, changes: changes) }

  describe "#execute" do
    context "without user" do
      let(:user) { nil }

      it "return false" do
        expect(ContentValidation::CommitServiceWorker).not_to receive(:perform_async)
        expect(subject.execute).to eq(false)
      end
    end

    context "with project changes" do
      context "with private project" do
        let(:project) { create(:project, :repository, :private) }

        it "return false" do
          expect(ContentValidation::CommitServiceWorker).not_to receive(:perform_async)
          expect(subject.execute).to eq(false)
        end
      end

      it "calls ContentValidation::CommitServiceWorker#perform_async" do
        expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
        subject.execute
      end

      context "branch changes" do
        context "create default branch" do
          it "calls ContentValidation::CommitServiceWorker#perform_async" do
            expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
            subject.execute
          end
        end

        context "create new branch" do
          let(:commit) do
            project.repository.create_file(
              project.creator,
              'README.md',
              "README on branch test",
              message: 'Add README.md',
              branch_name: "test")
            project.repository.commit("test")
          end

          let(:changes) { [{ oldrev: Gitlab::Git::BLANK_SHA, newrev: commit.id, ref: "refs/heads/test" }] }

          it "calls ContentValidation::CommitServiceWorker#perform_async" do
            expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
            subject.execute
          end
        end

        context "update branch" do
          let(:prev_commit) { project.repository.commits(default_branch, limit: 1, offset: 1)[0] }
          let(:changes) { [{ oldrev: prev_commit.id, newrev: commit.id, ref: "refs/heads/#{default_branch}" }] }

          it "calls ContentValidation::CommitServiceWorker#perform_async once" do
            # rubocop: disable CodeReuse/ActiveRecord
            expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).with(commit.id, "project-#{project.id}", user.id).once
            # rubocop: enable CodeReuse/ActiveRecord
            subject.execute
          end
        end

        context "remove branch" do
          let(:changes) { [{ oldrev: commit.id, newrev: Gitlab::Git::BLANK_SHA, ref: "refs/heads/#{default_branch}" }] }

          it "not calls ContentValidation::CommitServiceWorker#perform_async" do
            expect(ContentValidation::CommitServiceWorker).not_to receive(:perform_async)
            subject.execute
          end
        end
      end

      context "tag changes" do
        context "create new tag" do
          let(:commit) do
            project.repository.create_file(
              project.creator,
              'test.md',
              "test",
              message: 'test',
              branch_name: "test")
            project.repository.commit("test")
          end

          let(:changes) { [{ oldrev: Gitlab::Git::BLANK_SHA, newrev: commit.id, ref: "refs/tags/vtest" }] }

          before do
            Commits::TagService.new(project, user, { tag_name: "vtest", tag_message: "test" }).execute(commit)
          end

          it "calls ContentValidation::CommitServiceWorker#perform_async" do
            expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
            subject.execute
          end
        end
      end
    end

    context "with wiki changes" do
      let(:repo_type) { Gitlab::GlRepository::WIKI }
      let(:container) { wiki }
      let!(:wiki_page) { create(:wiki_page, wiki: wiki, container: project) }

      context "with private wiki container" do
        let(:project) { create(:project, :repository, :private) }
        let(:wiki) { build(:project_wiki, :empty_repo, project: project) }

        it "return false" do
          expect(ContentValidation::CommitServiceWorker).not_to receive(:perform_async)
          expect(subject.execute).to eq(false)
        end
      end

      context "with group wiki" do
        let(:project) { nil }
        let(:wiki) { build(:group_wiki, :empty_repo, container: group) }
        let!(:wiki_page) { create(:wiki_page, wiki: wiki, container: group) }

        it "calls ContentValidation::CommitServiceWorker#perform_async" do
          expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
          subject.execute
        end
      end

      context "with project wiki" do
        let(:wiki) { build(:project_wiki, :empty_repo, project: project) }

        it "calls ContentValidation::CommitServiceWorker#perform_async" do
          expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
          subject.execute
        end
      end
    end

    context "with snippet changes" do
      let(:repo_type) { Gitlab::GlRepository::SNIPPET }
      let(:container) { snippet }

      context "with private snippet" do
        let(:snippet) { create(:snippet, :repository, :private) }

        it "return false" do
          expect(ContentValidation::CommitServiceWorker).not_to receive(:perform_async)
          expect(subject.execute).to eq(false)
        end
      end

      context "with personal snippet" do
        let(:snippet) { create(:personal_snippet, :repository, :public) }

        it "calls ContentValidation::CommitServiceWorker#perform_async" do
          expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
          subject.execute
        end
      end

      context "with project snippet" do
        let(:snippet) { create(:project_snippet, :repository, :public) }

        it "calls ContentValidation::CommitServiceWorker#perform_async" do
          expect(ContentValidation::CommitServiceWorker).to receive(:perform_async).at_least(:once)
          subject.execute
        end
      end
    end
  end
end

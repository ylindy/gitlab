# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Projects::WikisController do
  describe 'GET #show' do
    render_views

    let_it_be(:user) { create(:user) }
    let_it_be(:project) { create(:project, :public, :repository) }
    let_it_be(:wiki) { create(:project_wiki, project: project) }
    let_it_be(:wiki_page) { create(:wiki_page, wiki: wiki) }

    context "content_validation" do
      before do
        allow(ContentValidation::Setting).to receive(:block_enabled?).and_return(true)
        sign_in(user)
      end

      context "with blocked file blob" do
        let!(:content_blocked_state) { create(:content_blocked_state, container: wiki, commit_sha: wiki_page.version.commit.id, path: wiki_page.path) }
        let(:blocked_message) { s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.") }

        it "render blocked message" do
          get :show, params: {
            namespace_id: project.namespace,
            project_id: project,
            id: wiki_page.path
          }

          expect(response.body).to include(blocked_message)
        end
      end
    end
  end
end

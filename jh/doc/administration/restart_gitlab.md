---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 如何重启极狐GitLab **(FREE SELF)**

根据您安装极狐GitLab 的方式，有不同的方法可以重新启动其服务。

## Omnibus 安装实例

如果你已经使用 [Omnibus 安装包](https://about.gitlab.cn/install/) 来安装极狐GitLab，那么您的 `PATH` 中应该已经有了 `gitlab-ctl`。

`gitlab-ctl` 与 Omnibus 安装包交互，可用于重新启动 GitLab Rails 应用程序 (Puma) 以及其它组件，例如：

- GitLab Workhorse
- Sidekiq
- PostgreSQL (如果您使用捆绑组件)
- NGINX (如果您使用捆绑组件)
- Redis (如果您使用捆绑组件)
<!--- [Mailroom](reply_by_email.md)-->
- Logrotate

### Omnibus GitLab restart

文档中可能有时会要求您 *restart* GitLab。 在这种情况下，您需要运行以下命令：

```shell
sudo gitlab-ctl restart
```

输出应与此类似：

```plaintext
ok: run: gitlab-workhorse: (pid 11291) 1s
ok: run: logrotate: (pid 11299) 0s
ok: run: mailroom: (pid 11306) 0s
ok: run: nginx: (pid 11309) 0s
ok: run: postgresql: (pid 11316) 1s
ok: run: redis: (pid 11325) 0s
ok: run: sidekiq: (pid 11331) 1s
ok: run: puma: (pid 11338) 0s
```

要单独重新启动组件，您可以将其服务名称附加到 `restart` 命令。例如，要*仅*重新启动 NGINX，您应运行：

```shell
sudo gitlab-ctl restart nginx
```

要检查 GitLab 服务的状态，请运行：

```shell
sudo gitlab-ctl status
```

请注意，所有服务应显示 `ok: run`。

有时，组件在重启期间超时（在日志中查找 `timeout`），有时它们会卡住。在这种情况下，您可以使用 `gitlab-ctl kill <service>` 将 `SIGKILL` 信号发送到服务，例如 `sidekiq`。之后，重新启动应该可以正常工作。

作为最后的手段，您可以尝试[重新配置极狐 GitLab](#omnibus-gitlab-reconfigure) 作为代替。

### Omnibus GitLab reconfigure

文档中可能有时会要求您*重新配置*极狐GitLab。请记住，此方法仅适用于 Omnibus 安装包。

使用以下命令重新配置 Omnibus GitLab：

```shell
sudo gitlab-ctl reconfigure
```

如果配置 (`/etc/gitlab/gitlab.rb`) 中的某些内容发生更改，则应该重新配置极狐GitLab。

当您运行此命令时，[Chef](https://www.chef.io/products/chef-infra)（为 Omnibus GitLab 提供支持的底层配置管理应用程序）确保目录、权限和服务等所有内容都已就位，并且与最初交付时的形状相同。

如果任何配置文件发生更改，它还会在需要时重新启动 GitLab 组件。

如果您手动编辑由 Chef 管理的 `/var/opt/gitlab` 中的任何文件，则运行 reconfigure 将恢复更改并重新启动依赖于这些文件的服务。


## 源安装实例

如果您已经从源代码安装<!--按照官方安装指南[从源代码安装 GitLab](../install/installation.md)-->，请运行以下命令重新启动 GitLab：

```shell
# For systems running systemd
sudo systemctl restart gitlab.target

# For systems running SysV init
sudo service gitlab restart
```

这应该会重新启动 Puma、Sidekiq、GitLab Workhorse 和 Mailroom<!--[Mailroom](reply_by_email.md)-->（如果已启用）。

## Helm chart 安装实例

没有一个命令可以重新启动通过[云原生 Helm Chart](https://docs.gitlab.cn/charts/) 安装的整个 GitLab 应用程序。通常，通过删除与其相关的所有 pod 来单独重新启动特定组件（例如，`gitaly`、`puma`、`workhorse` 或 `gitlab-shell`）就足够了：

```shell
kubectl delete pods -l release=<helm release name>,app=<component name>
```

发布名称可以从 `helm list` 命令的输出中获得。

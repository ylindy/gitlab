---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 有用的 Git 命令 **(FREE)**

以下命令可能可以帮助到您。您可能不需要经常使用它们。

## 远端

### 将另一个 URL 添加到远端，以便在每次推送时更新两个远端

```shell
git remote set-url --add <remote_name> <remote_url>
```

## 暂存和恢复更改

### 删除最后一次提交并将更改保留在未暂存状态

```shell
git reset --soft HEAD^
```

### 从 HEAD 取消一定数量的提交

To unstage 3 commits, for example, run:

```shell
git reset HEAD^3
```

### 从 HEAD 取消对某个文件的暂存更改

```shell
git reset <filename>
```

### 将文件恢复为 HEAD 状态并删除更改

有两个选项可以恢复对文件的更改：

- `git checkout <filename>`
- `git reset --hard <filename>`

### 通过创建新的替换提交来撤消之前的提交

```shell
git revert <commit-sha>
```

### 为上次提交创建一条新消息

```shell
git commit --amend
```

### 将文件添加到最后一次提交

```shell
git add <filename>
git commit --amend
```

如果您不想编辑提交消息，请将 `--no-edit` 附加到 `commit` 命令。

## 隐藏

### 隐藏更改

```shell
git stash save
```

`stash` 的默认行为是保存，所以您也可以只使用：

```shell
git stash
```

### 取消隐藏您的更改

```shell
git stash apply
```

### 丢弃您隐藏的更改

```shell
git stash drop
```

### 应用和删除您隐藏的更改

```shell
git stash pop
```

## Refs 和日志

### 使用 reflog 显示对 HEAD 的引用更改日志

```shell
git reflog
```

### 检查文件的 Git 历史记录

检查文件的 Git 历史记录的基本命令：

```shell
git log <file>
```

如果您收到此错误消息：

```plaintext
fatal: ambiguous argument <file_name>: unknown revision or path not in the working tree.
Use '--' to separate paths from revisions, like this:
```

使用它来检查文件的 Git 历史记录：

```shell
git log -- <file>
```

### 查找包含特定 SHA 的标签

```shell
git tag --contains <sha>
```

### 检查对文件的每次更改的内容

```shell
gitk <file>
```

### 检查每个文件更改的内容，跟随它过去的文件重命名

```shell
gitk --follow <file>
```

## Debugging

### 为 Git 命令使用自定义 SSH 密钥

```shell
GIT_SSH_COMMAND="ssh -i ~/.ssh/gitlabadmin" git <command>
```

### Debug 克隆

使用 SSH：

```shell
GIT_SSH_COMMAND="ssh -vvv" git clone <git@url>
```

使用 HTTPS：

```shell
GIT_TRACE_PACKET=1 GIT_TRACE=2 GIT_CURL_VERBOSE=1 git clone <url>
```

### 使用 Git 嵌入式跟踪进行调试

Git 包含一套完整的[调试 Git 命令的跟踪](https://git-scm.com/book/en/v2/Git-Internals-Environment-Variables#_debugging)，例如：

- `GIT_TRACE_PERFORMANCE=1`：启用性能数据的跟踪，显示每个特定的 `git` 调用需要多长时间。
- `GIT_TRACE_SETUP=1`：启用跟踪 `git` 正在发现的关于它正在交互的仓库和环境的内容。
- `GIT_TRACE_PACKET=1`：为网络操作启用数据包级跟踪。

## 变基

### 将您的分支变基到默认分支上

`-i` 标志代表“交互式”。将 `<default-branch>` 替换为您的默认分支<!--[默认分支](../../user/project/repository/branches/default.md)-->的名称：

```shell
git rebase -i <default-branch>
```

### 如果暂停，继续变基

```shell
git rebase --continue
```

### 使用 `git rerere`

在重复时*重用*记录的相同问题的解决方案：

```shell
git rerere
```

要启用 `rerere` 功能：

```shell
git config --global rerere.enabled true
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

---
stage: Create
group: Source Code
info: 要确定分配给与此页面关联的 Stage/Group 的技术作者，请参阅 https://about.gitlab.com/handbook/engineering/ux/technical-writing/# assignments 
---

# 标签 **(FREE)**

标签对于标记某些部署和发布供以后参考很有用。Git 支持两种类型的标签：

- 带注释的标签：Git 历史中不可更改的部分。
- 轻量（软）标签：可以根据需要设置和删除的标签。

许多项目将带注释的发布标签与稳定分支结合在一起。考虑自动设置部署或发布标签。

## 标签示例工作流程

1. 创建一个轻量级标签。
1. 创建一个带注释的标签。
1. 将标签推送到远端仓库。

```shell
git checkout master

# 轻量级标签
git tag my_lightweight_tag

# 带注释的标签
git tag -a v1.0 -m 'Version 1.0'

# 显示现有标签列表
git tag

git push origin --tags
```

## 附加资源

- [标签](https://git-scm.com/book/en/v2/Git-Basics-Tagging) Git 参考页面

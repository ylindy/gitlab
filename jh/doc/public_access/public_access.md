---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 项目和群组可见性 **(FREE)**

允许拥有所有者角色的用户将项目或群组的可见性设置为：

- **公开**
- **内部**
- **私有**

这些可见性级别影响谁可以看到公共访问目录中的项目（实例的`/public`）。例如，<https://gitlab.cn/public>。您可以使用项目功能设置<!--[项目功能设置](../user/permissions.md#project-features)-->控制单个功能的可见性。

## 公开项目和群组

**无需任何** HTTPS 身份验证即可克隆公共项目。

它们列在所有用户的公共访问目录 (`/public`) 中。

**任何登录用户** 在仓库中都具有访客角色。

NOTE:
默认情况下，未经身份验证的用户可以看到 `/public`。但是，如果<!--[**公开** 可见性级别](../user/admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->**公开** 可见性级别受到限制，则 `/public` 仅对登录用户可见。

## 内部项目和群组

除了外部用户<!--[外部用户](../user/permissions.md#external-users)-->之外，任何登录用户都可以克隆内部项目。

它们也列在公共访问目录 (`/public`) 中，但仅适用于登录用户。

除<!--[外部用户](../user/permissions.md#external-users)-->外部用户之外的任何登录用户在仓库中都具有访客角色<!--[角色](../user/permissions.md)-->。

<!--
NOTE:
From July 2019, the `Internal` visibility setting is disabled for new projects, groups,
and snippets on GitLab.com. Existing projects, groups, and snippets using the `Internal`
visibility setting keep this setting. You can read more about the change in the
[relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/12388).-->

## 私有项目和群组

私有项目只能被项目成员（访客除外）克隆和查看。

它们仅出现在项目成员的公共访问目录 (`/public`) 中。

## 更改项目可见性

先决条件：

- 您必须拥有项目的所有者角色。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开  **可见性, 项目功能, 权限**。
1. 将 **项目可见性** 更改为 **私有**、**内部** 或 **公开**。
1. 选择 **保存修改**。

## 更改群组可见性

先决条件：

- 您必须拥有群组的所有者角色。

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **名称与可见性**。
1. 在 **可见性级别** 下，选择 **私有**、**内部** 或 **公开**。
1. 选择 **保存修改**。

## 限制使用公共或内部项目 **(FREE SELF)**

您可以在用户创建项目或代码片段时限制他们使用的可见性级别。
这有助于防止用户意外公开他们的仓库。受限可见性设置不适用于管理员。

<!--For details, see [Restricted visibility levels](../user/admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels).-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

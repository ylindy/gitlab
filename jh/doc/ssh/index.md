---
stage: Manage
group: Access
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: howto, reference
---

# 极狐GitLab 和 SSH 密钥 **(FREE)**

Git 是一个分布式版本控制系统，这意味着您可以在本地工作，然后将您的更改共享或“推送”到服务器。在这种情况下，服务器是极狐GitLab。

极狐GitLab 使用 SSH 协议来安全地与 Git 通信。当您使用 SSH 密钥对 GitLab 远程服务器进行身份验证时，您无需每次都提供用户名和密码。

## 先决条件

要使用 SSH 与极狐GitLab 通信，您需要：

- OpenSSH 客户端，预装在 GNU/Linux、macOS 和 Windows 10 上。
- SSH 6.5 或更高版本。早期版本使用 MD5 签名，这是不安全的。

要查看系统上安装的 SSH 版本，请运行 `ssh -V`。

## 支持的 SSH 密钥类型

要与极狐GitLab 通信，您可以使用以下 SSH 密钥类型：

- [ED25519](#ed25519-ssh-密钥)
- [RSA](#rsa-ssh-密钥)
- DSA (已于 11.0 版本废弃)
- ECDSA (如 [Practical Cryptography With Go](https://leanpub.com/gocrypto/read#leanpub-auto-ecdsa) 中所述，与 DSA 相关的安全问题也适用于 ECDSA。)

管理员可以限制允许使用哪些密钥及其最小长度<!--[限制允许使用哪些密钥及其最小长度](../security/ssh_keys_restrictions.md)-->。

### ED25519 SSH 密钥

[Practical Cryptography With Go](https://leanpub.com/gocrypto/read#leanpub-auto-chapter-5-digital-signatures) 一书表明 [ED25519](https://ed25519.cr.yp.to/) 密钥比 RSA 密钥更安全、更高效。

OpenSSH 6.5 在 2014 年引入了 ED25519 SSH 密钥，它们应该可以在大多数操作系统上使用。

### RSA SSH 密钥

可用文档表明 ED25519 比 RSA 更安全。

如果您使用 RSA 密钥，美国国家科学技术研究院 [Publication 800-57 Part 3 (PDF)](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-57Pt3r1.pdf) 建议密钥大小至少为 2048 位。默认密钥大小取决于您的 `ssh-keygen` 版本。有关详细信息，请查看已安装的 ssh-keygen 命令的手册页。

## 查看您是否已有 SSH 密钥对

在创建密钥对之前，请查看密钥对是否已存在。

1. 在 Windows、Linux 或 macOS 上，转到您的主目录。
1. 进入`.ssh/` 子目录。如果 `.ssh/` 子目录不存在，您可能不在主目录中，或者可能之前没有使用过 `ssh`。在后一种情况下，您需要[生成 SSH 密钥对](#生成-ssh-密钥对)。
1. 查看是否存在以下格式之一的文件：

   | 算法 | 公钥 | 私钥 |
   | --------- | ---------- | ----------- |
   |  ED25519 (首选)  | `id_ed25519.pub` | `id_ed25519` |
   |  RSA (至少 2048 位密钥大小)     | `id_rsa.pub` | `id_rsa` |
   |  DSA (废弃)      | `id_dsa.pub` | `id_dsa` |
   |  ECDSA    | `id_ecdsa.pub` | `id_ecdsa` |

## 生成 SSH 密钥对

如果您没有现有的 SSH 密钥对，请生成一个新的。

1. 打开一个终端。
1. 输入 `ssh-keygen -t`，然后输入密钥类型和可选注释。此注释包含在创建的 `.pub` 文件中。您可能希望在注释中使用电子邮件地址。

   例如，对于 ED25519：

   ```shell
   ssh-keygen -t ed25519 -C "<comment>"
   ```

   对于 2048 位 RSA：

   ```shell
   ssh-keygen -t rsa -b 2048 -C "<comment>"
   ```

1. 按 Enter。将显示类似于以下内容的输出：

   ```plaintext
   Generating public/private ed25519 key pair.
   Enter file in which to save the key (/home/user/.ssh/id_ed25519):
   ```

1. 接受建议的文件名和目录，除非您正在生成部署密钥<!--[部署密钥](../user/project/deploy_keys/index.md)-->，或想要保存在存储其他密钥的特定目录中。

  您还可以将 SSH 密钥对专用于[特定主机](#配置-ssh-以指向不同的目录)。

1. 指定[密码](https://www.ssh.com/academy/ssh/passphrase)：

   ```plaintext
   Enter passphrase (empty for no passphrase):
   Enter same passphrase again:
   ```

1. 将显示确认信息，包括有关文件存储位置的信息。

生成公钥和私钥。[将公共 SSH 密钥添加到您的极狐GitLab 帐户](#将-ssh-密钥添加到您的极狐gitlab-帐户)，并确保私钥安全。

### 配置 SSH 以指向不同的目录

如果您没有将 SSH 密钥对保存在默认目录中，请将您的 SSH 客户端配置为指向存储私钥的目录。

1. 打开终端并运行以下命令：

   ```shell
   eval $(ssh-agent -s)
   ssh-add <directory to private SSH key>
   ```

1. 将这些设置保存在 `~/.ssh/config` 文件中。 例如：

   ```conf
   # GitLab.com
   Host gitlab.com
     PreferredAuthentications publickey
     IdentityFile ~/.ssh/gitlab_com_rsa

   # Private GitLab instance
   Host gitlab.company.com
     PreferredAuthentications publickey
     IdentityFile ~/.ssh/example_com_rsa
   ```

  有关这些设置的更多信息，请参阅 SSH 配置手册中的 [`man ssh_config`](https://man.openbsd.org/ssh_config) 页面。

公共 SSH 密钥对于极狐GitLab 必须是唯一的，因为它们绑定到您的帐户。当您使用 SSH 推送代码时，您的 SSH 密钥是您拥有的唯一标识符。它必须唯一地映射到单个用户。

### 更新您的 SSH 密钥密码

您可以更新 SSH 密钥的密码。

1. 打开终端并运行以下命令：

   ```shell
   ssh-keygen -p -f /path/to/ssh_key
   ```

1. 在出现提示时，输入密码并按 Enter。

### 将您的 RSA 密钥对升级为更安全的格式

如果您的 OpenSSH 版本介于 6.5 和 7.8 之间，您可以以更安全的 OpenSSH 格式保存您的私有 RSA SSH 密钥。

1. 打开终端并运行以下命令：

   ```shell
   ssh-keygen -o -f ~/.ssh/id_rsa
   ```

   或者，您可以使用以下命令生成具有更安全加密格式的新 RSA 密钥：

   ```shell
   ssh-keygen -o -t rsa -b 4096 -C "<comment>"
   ```

## 将 SSH 密钥添加到您的极狐GitLab 帐户

要将 SSH 与极狐GitLab 结合使用，请将您的公钥复制到您的极狐GitLab 帐户。

1. 复制公钥文件的内容。您可以手动执行此操作或使用脚本。例如，要将 ED25519 密钥复制到剪贴板：

   **macOS：**

   ```shell
   tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
   ```

   **Linux** (需要 `xclip` 包)：

   ```shell
   xclip -sel clip < ~/.ssh/id_ed25519.pub
   ```

   **Git Bash on Windows：**

   ```shell
   cat ~/.ssh/id_ed25519.pub | clip
   ```

   将 `id_ed25519.pub` 替换为您的文件名。例如，对 RSA 使用 `id_rsa.pub`。

1. 登录极狐GitLab。
1. 在顶部栏的右上角，选择您的头像。
1. 选择 **偏好设置**。
1. 在左侧边栏上，选择**SSH 密钥**。
1. 在 **密钥** 框中，粘贴公钥的内容。如果您手动复制密钥，请确保复制整个密钥，以 `ssh-ed25519` 或 `ssh-rsa` 开头，并可能以注释结尾。
1. 在 **标题** 框中，输入说明，例如 “Work Laptop” 或 “Home Workstation”。
1. 可选。在 **到期于** 框中，选择过期日期。（引入于 12.9 版本）

   - 对于 13.12 及更早版本，到期日期仅供参考。它不会阻止您使用密钥。管理员可以查看到期日期并在删除密钥<!--[删除密钥](../user/admin_area/credentials_inventory.md#delete-a-users-ssh-key)-->时作为指导。
   - 对于 14.0 及更高版本，强制执行到期日期。管理员可以允许使用过期密钥<!--[允许使用过期密钥](../user/admin_area/settings/account_and_limit_settings.md#allow-expired-ssh-keys-to-be-used)-->。
   - 每天 02:00 AM UTC 检查所有 SSH 密钥。通过电子邮件发送所有在当前日期到期的 SSH 密钥的到期通知。（引入于 13.11 版本）
   - 每天 01:00 AM UTC 检查所有 SSH 密钥。通过电子邮件发送所有计划在 7 天后到期的 SSH 密钥的到期通知。（引入于 13.11 版本）

1. 选择 **添加密钥**。

## 验证您是否可以连接

验证您的 SSH 密钥是否已正确添加。

<!--1. For GitLab.com, to ensure you're connecting to the correct server, confirm the
   [SSH host keys fingerprints](../user/gitlab_com/index.md#ssh-host-keys-fingerprints).-->
1. 打开终端并运行此命令，将 `gitlab.example.com` 替换为您的实例 URL：

   ```shell
   ssh -T git@gitlab.example.com
   ```

1. 如果这是您第一次连接，您应该验证 GitLab 主机的真实性。如果您看到如下消息：

   ```plaintext
   The authenticity of host 'gitlab.example.com (35.231.145.151)' can't be established.
   ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
   Are you sure you want to continue connecting (yes/no)? yes
   Warning: Permanently added 'gitlab.example.com' (ECDSA) to the list of known hosts.
   ```

   输入 `yes`，然后按 Enter。

1. 再次运行 `ssh -T git@gitlab.example.com` 命令。您应该会收到 *Welcome to GitLab, `@username`!* 消息。

如果没有出现欢迎消息，您可以通过在详细模式下运行 `ssh` 来进行故障排除：

```shell
ssh -Tvvv git@gitlab.example.com
```

## 对不同的仓库使用不同的密钥

您可以为每个仓库使用不同的密钥。

打开终端并运行以下命令：

```shell
git config core.sshCommand "ssh -o IdentitiesOnly=yes -i ~/.ssh/private-key-filename-for-this-repository -F /dev/null"
```

此命令不使用 SSH 代理，需要 Git 2.10 或更高版本。有关 `ssh` 命令选项的更多信息，请参阅 `ssh` 和 `ssh_config` 的 `man` 页。

## 在单个实例上使用不同的帐户

您可以使用多个帐户连接到 GitLab 的单个实例。
您可以使用[前一节](#对不同的仓库使用不同的密钥)中的命令来执行此操作。但是，即使您将 `IdentitiesOnly` 设置为 `yes`，如果 `IdentityFile` 存在于 `Host` 块之外，您也无法登录。

相反，您可以在`~.ssh/config` 文件中为主机分配别名。

- 对于 `Host`，使用像 `user_1.gitlab.cn` 和 `user_2.gitlab.cn` 这样的别名。高级配置更难维护，当使用像 `git remote`这样的工具时，这些字符串更容易理解。
- 对于 `IdentityFile`，使用私钥的路径。

```conf
# User1 Account Identity
Host <user_1.gitlab.cn>
  Hostname gitlab.cn
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/<example_ssh_key1>

# User2 Account Identity
Host <user_2.gitlab.cn>
  Hostname gitlab.cn
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/<example_ssh_key2>
```

现在，要为 `user_1` 克隆仓库，请在 `git clone` 命令中使用 `user_1.gitlab.cn`：

```shell
git clone git@<user_1.gitlab.cn>:gitlab-cn/gitlab.git
```

要更新别名为 `origin` 的先前克隆的存储库：

```shell
git remote set-url origin git@<user_1.gitlab.com>:gitlab-org/gitlab.git
```

NOTE:
私钥和公钥包含敏感数据。 确保文件的权限使您可以读取但其他人无法访问。

## 配置双重认证（2FA）

您可以为 Git over SSH<!--[Git over SSH](../security/two_factor_authentication.md#2fa-for-git-over-ssh-operations)--> 设置双因素身份验证 (2FA)。

## 在 Eclipse 上使用 EGit

如果您使用 [EGit](https://www.eclipse.org/egit/)，您可以[将您的 SSH 密钥添加到 Eclipse](https://wiki.eclipse.org/EGit/User_Guide#Eclipse_SSH_Configuration)。

## 在 Microsoft Windows 上使用 SSH

如果您运行的是 Windows 10，则可以将 [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/install) 与预装了 `git` 和 `ssh`  的 [WSL 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10#update-to-wsl-2) 一起使用 ，或者安装 [Git for Windows](https://gitforwindows.org)，通过 Powershell 使用 SSH。

WSL 中生成的 SSH 密钥不能直接用于 Windows 版 Git，反之亦然，因为两者都有不同的主目录：

- WSL: `/home/<user>`
- Git for Windows: `C:\Users\<user>`

您可以复制 `.ssh/` 目录以使用相同的密钥，也可以在每个环境中生成一个密钥。

替代工具包括：

- [Cygwin](https://www.cygwin.com)
- [PuttyGen](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)

## 覆盖 GitLab 服务器上的 SSH 设置

极狐GitLab 与系统安装的 SSH 守护进程集成，并指定一个用户（通常名为“git”），通过该用户处理所有访问请求。通过 SSH 连接到 GitLab 服务器的用户由其 SSH 密钥而不是用户名标识。

在 GitLab 服务器上执行的 SSH *客户端*操作以此用户身份执行。您可以修改此 SSH 配置。例如，您可以为此用户指定用于身份验证请求的私有 SSH 密钥。但是，这种做法**不受支持**，强烈建议不要这样做，因为它会带来重大的安全风险。

系统会检查这种情况，<!--，如果您的服务器以这种方式配置，则会将您定向到此部分。-->例如：

```shell
$ gitlab-rake gitlab:check

Git user has default SSH configuration? ... no
  Try fixing it:
  mkdir ~/gitlab-check-backup-1504540051
  sudo mv /var/lib/git/.ssh/id_rsa ~/gitlab-check-backup-1504540051
  sudo mv /var/lib/git/.ssh/id_rsa.pub ~/gitlab-check-backup-1504540051
  For more information see:
  [Overriding SSH settings on the GitLab server](#overriding-ssh-settings-on-the-gitlab-server)
  Please fix the error above and rerun the checks.
```

尽快删除自定义配置。这些自定义设置**明确不受支持**，并且可能随时停止工作。

## SSH 连接故障排查

当您运行 `git clone` 时，可能会提示输入密码，比如 `git@gitlab.example.com's password:`。这表明您的 SSH 设置有问题。

- 确保您正确生成了 SSH 密钥对，并将公共 SSH 密钥添加到您的 GitLab 配置文件中。
- 尝试使用 `ssh-agent` 手动注册您的私有 SSH 密钥。
- 尝试通过运行 `ssh -Tv git@example.com` 来调试连接。用您的 GitLab URL 替换 `example.com`。

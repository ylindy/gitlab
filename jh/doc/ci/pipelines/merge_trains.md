---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
last_update: 2019-07-03
---

# 合并队列 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9186) in GitLab 12.0.
> - [Squash and merge](../../user/project/merge_requests/squash_and_merge.md) support [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13001) in GitLab 12.6.

For more information about why you might want to use merge trains, read [How merge trains keep your master green](https://about.gitlab.com/blog/2020/01/30/all-aboard-merge-trains/).
-->

当[合并结果流水线](pipelines_for_merged_results.md)启用时，流水线作业运行就像来自源分支的更改已经合并到目标分支一样。

然而，目标分支可能正在迅速变化。当您准备好合并时，如果您有一段时间没有运行流水线，则目标分支可能已经更改。
现在合并可能会带来重大变化。

*合并队列*可以防止这种情况发生。合并队列是合并请求的排队列表，每个请求都在等待合并到目标分支中。

许多合并请求可以添加到队列中。每个合并请求都运行自己的合并结果流水线，其中包括队列上*前面*的所有其他合并请求的更改。
所有流水线并行运行，节省时间。

如果合并请求的流水线失败，则不会合并破坏性更改，并且目标分支不受影响。合并请求从队列中移除，并且它后面的所有流水线重新启动。

如果队列前端合并请求的流水线成功完成，则将更改合并到目标分支中，其它流水线继续运行。

要将合并请求添加到合并队列，您需要推送到目标分支的权限。

每个合并队列最多可以并行运行 **20** 个流水线。
如果将超过 20 个合并请求添加到合并队列，合并请求将排队，直到合并队列中的一个槽空闲。可以排队的合并请求的数量没有限制。

## 合并队列示例

三个合并请求（`A`、`B` 和`C`）按顺序添加到一个合并队列中，这会创建三个并行运行的合并结果流水线：

1. 第一个流水线运行在`A` 与目标分支相结合的变化上。
1. 第二个流水线运行在 `A` 和 `B` 的变化以及目标分支上。
1. 第三个流水线运行在`A`、`B` 和`C` 与目标分支相结合的变化上。

如果 `B` 的流水线出现故障，则将其从队列中移除。`C` 的流水线随着 `A` 和 `C` 的变化而重新启动，但没有 `B` 的变化。

如果 `A` 然后成功完成，它会合并到目标分支，并且 `C` 继续运行。如果将更多合并请求添加到队列中，它们现在包括目标分支中包含的 `A` 更改，以及来自队列中已存在的合并请求的 `C` 更改。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
Watch this video for a demonstration on [how parallel execution
of merge trains can prevent commits from breaking the default
branch](https://www.youtube.com/watch?v=D4qCqXgZkHQ).
-->

## 先决条件

要启用合并队列：

- 您必须具有维护者角色。
- 您必须使用 GitLab Runner 11.9 或更高版本。
- 在 13.0 及更高版本中，您需要 [Redis](https://redis.io/) 5.0 或更高版本。
- 您的仓库必须是极狐GitLab 仓库，而不是外部仓库<!--[外部仓库](../ci_cd_for_external_repos/index.md)-->。

## 启用合并队列

要为您的项目启用合并队列：

1. 如果您使用的是自助管理实例，请确保正确设置[功能标志](#合并队列功能标志)。
1. [配置您的 CI/CD 配置文件](merge_request_pipelines.md#为合并请求配置流水线) 以便流水线或单个作业针对合并请求运行。
1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并方法** 部分，确认 **合并提交** 被选中。您不能使用 **合并提交与半线性历史记录** 或为合并队列使用 **快进式合并**。
1. 在 **合并选项** 部分，选择 **启用合并结果流水线**（如果尚未选择）和 **启用合并队列**。
1. 选择 **保存修改**。

在 13.5 及更早版本中，只有一个复选框，名为 **为合并结果启用合并队列和流水线**。

WARNING:
如果选中该复选框但未将 CI/CD 配置为将流水线用于合并请求，则合并请求可能会卡在未解决状态或流水线可能会被丢弃。

## 启动合并队列

要启动合并队列：

1. 访问合并请求。
1. 选择 **启动合并队列**。

![Start merge train](img/merge_train_start_v12_0.png)

现在可以将其他合并请求添加到队列中。

## 向合并队列添加合并请求

向合并队列添加合并请求：

1. 访问合并请求。
1. 选择 **添加到合并队列**。

如果合并请求的流水线已经在运行，则不能将合并请求添加到队列中。相反，**当最新的流水线成功时**，您可以安排将合并请求添加到合并队列。

![Add to merge train when pipeline succeeds](img/merge_train_start_when_pipeline_succeeds_v12_0.png)

## 从合并队列中删除合并请求

1. 访问合并请求。
1. 选择 **从合并队列中移除**。

![Cancel merge train](img/merge_train_cancel_v12_0.png)

您可以稍后再次将合并请求添加到合并列车。

## 查看合并请求在合并队列中的当前位置

将合并请求添加到合并队列后，合并请求的当前位置将显示在流水线部件下方：

![Merge train position indicator](img/merge_train_position_v12_0.png)

## 立即将合并请求与合并队列合并

如果您有一个必须紧急合并的高优先级合并请求（例如，一个关键补丁），您可以使用 **立即合并** 选项绕过合并队列。
这是将更改合并到目标分支的最快选项。

![Merge Immediately](img/merge_train_immediate_merge_v12_6.png)

WARNING:
每次立即合并合并请求时，都会重新创建当前合并队列，并重新启动所有流水线。

## 故障排查

### 立即从合并队列中删除合并请求

如果合并请求不可合并（例如，它是 Draft 合并请求或存在合并冲突），合并队列会自动丢弃您的合并请求。

在这些情况下，删除合并请求的原因在 **系统注释** 中。

要检查原因：

1. 打开从合并队列中丢弃的合并请求。
1. 选择 **讨论** 选项卡。
1. 找到包含以下任一内容的系统注释：
    - **...从合并队列中删除了这个合并请求，因为...**
    - **...从合并队列中止了这个合并请求，因为...**

原因在正文中的 **因为...** 短语之后给出。

![Merge train failure](img/merge_train_failure.png)

### 无法选择流水线成功时合并

[当流水线成功时合并](../../user/project/merge_requests/merge_when_pipeline_succeeds.md)当前在启用合并队列时不可用。

<!--
See [the related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/12267)
for more information.
-->

### 无法重试合并队列流水线

当合并队列的流水线失败时，合并请求将从队列中删除，并且无法重试流水线。
合并队列的流水线运行在合并请求中更改的合并结果，和队列中已有的其他合并请求的更改上。如果合并请求从队列中删除，则合并结果已过期，并且无法重试流水线。

相反，您应该再次[将合并请求添加到队列](#向合并队列添加合并请求)，这会触发新的流水线。

### 无法添加到合并队列 "The pipeline for this merge request failed."

有时 **启动/添加到合并队列** 按钮不可用并且合并请求提示，“The pipeline for this merge request failed. Please retry the job or push a new commit to fix the failure.”

当[**流水线必须成功**](../../user/project/merge_requests/merge_when_pipeline_succeeds.md#仅流水线成功允许合并合并请求) 在 **设置 > 通用 > 合并请求** 中启用。此选项要求您运行新的成功流水线，然后才能将合并请求重新添加到合并队列。

合并队列确保每个流水线在合并之前都已成功，因此您可以清除 **流水线必须成功** 复选框并保持选中 **为合并结果启用合并队列和流水线**（合并队列）。

如果要保持 **流水线必须成功** 选项与合并队列一起选择，请在发生此错误时为合并结果创建新流水线：

1. 在 **流水线** 选项卡上，选择 **运行流水线**。
1. 选择 **Start/Add to merge train when pipeline success**。

<!--
See [the related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/35135)
for more information.
-->

### 合并队列功能标志 **(PREMIUM SELF)**

在 13.6 及更高版本中，您可以[在项目设置中启用或禁用合并列车](#启用合并队列)。

在 13.5 及更早版本中，当启用[合并结果的流水线](pipelines_for_merged_results.md) 时，会自动启用合并队列。
要在不使用合并队列的情况下将流水线用于合并结果，您可以启用阻止合并队列功能的功能标志<!--[功能标志](../../user/feature_flags.md)-->。

<!--[可以访问 GitLab Rails 控制台的管理员](../../administration/feature_flags.md)-->可以访问 GitLab Rails 控制台的管理员可以启用功能标志来禁用合并队列：

```ruby
Feature.enable(:disable_merge_trains)
```

启用此功能标志后，所有现有合并队列将被取消，**Start/Add to merge train** 按钮不再出现在合并请求中。

要禁用功能标志，并再次启用合并队列：

```ruby
Feature.disable(:disable_merge_trains)
```

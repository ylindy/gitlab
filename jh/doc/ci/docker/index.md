---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
type: index
---

# Docker 集成 **(FREE)**

将 [Docker](https://www.docker.com) 合并到您的 CI/CD 工作流程中有两种主要方法：

- **在 Docker 容器中[运行您的 CI/CD 作业](using_docker_images.md)。**

  您可以创建 CI/CD 作业来执行测试、构建或发布应用程序等操作。这些作业可以在 Docker 容器中运行。

  例如，您可以告诉 GitLab CI/CD 使用托管在 Docker Hub 或 GitLab 容器镜像库中的节点镜像。然后，您的作业在基于镜像的容器中运行。该容器具有构建应用程序所需的所有 Node 依赖项。

- **使用 [Docker](using_docker_build.md) 或 [kaniko](using_kaniko.md) 构建 Docker 镜像。**

  您可以创建 CI/CD 作业来构建 Docker 镜像并将它们发布到容器镜像库。

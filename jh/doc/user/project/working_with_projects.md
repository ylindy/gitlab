---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 管理项目 **(FREE)**

极狐GitLab 中的大部分工作都是在[项目](../../user/project/index.md)中完成的。文件和代码保存在项目中，大部分功能都在项目范围内。

## 查看项目

探索项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **浏览项目**。

**项目** 页面显示项目列表，按上次更新日期排序。

- 要查看具有最多[星标](#星标项目)的项目，请选择 **最多星标**。
- 要查看过去一个月评论数量最多的项目，请选择 **热门**。

NOTE:
未经身份验证的用户可以看到 **浏览项目** 选项卡，除非限制了**公开**可见性级别<!--[**公开**可见性级别](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->，然后该选项卡仅对登录用户可见。

### 谁可以查看 **项目** 页面

当您选择一个项目时，项目登录页面会显示项目内容。

对于公开项目，以及有权查看项目代码的内部和私有项目成员，项目登陆页面显示：

- [`README` 或索引文件](repository/index.md#readme-和-index-文件)。
- 项目仓库中的目录列表。

对于没有权限查看项目代码的用户，登陆页面显示：

- wiki 主页。
- 项目中的议题列表。

### 使用项目 ID 访问项目页面

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/53671) in GitLab 11.8.
-->

要使用项目 ID 从 GitLab UI 访问项目，请在浏览器或其他访问项目的工具中访问 `/projects/:id` URL。

## 浏览主题

要浏览项目主题：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **浏览主题**。

**项目** 页面显示按相关项目数量排序的主题列表。
要查看与主题关联的项目，请从列表中选择一个主题。

您可以在项目设置页面<!--[项目设置页面](settings/index.md#topics)-->上为项目分配主题。

如果您是实例管理员，则可以从管理中心的主题页面<!--[管理中心的主题页面](../admin_area/index.md#administering-topics)-->，管理所有项目主题。

## 创建项目

要在极狐GitLab 中创建项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **创建新项目**。
1. 在 **新建项目** 页面，选择是否：
   - 创建一个[空白项目](#创建空白项目)。
   - 从以下位置创建项目：
      - [内置模板](#从内置模板创建项目)。
      - [自定义模板](#从自定义模板创建项目)。  
      - [HIPAA 审计协议模板](#从-hipaa-审计协议模板创建项目)。
   - 从不同的仓库[导入项目](../../user/project/import/index.md)。如果此选项不可用，请联系您的 GitLab 管理员。
   - 将外部仓库连接到 GitLab CI/CD。<!--[将外部仓库连接到 GitLab CI/CD](../../ci/ci_cd_for_external_repos/index.md)。-->

NOTE:
有关不能用作项目名称的单词列表，请参阅[保留的项目和群组名称](../../user/reserved_names.md)。

## 创建空白项目

要创建空白项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **创建新项目**。
1. 选择 **创建空白项目**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。您可以使用空格、连字符、下划线和表情符号。不能使用特殊字符。输入名称后，将填入 **项目标识串**。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 在 **项目描述（可选）** 字段中，输入项目仪表板的描述。
   - 要为用户修改项目的[查看和访问权限](../../public_access/public_access.md)，请更改 **可见性级别**。
   - 要创建 README 文件以便初始化 Git 仓库、具有默认分支并可以克隆，请选择 **使用自述文件初始化仓库**。
   - 要分析项目中的源代码是否存在已知安全漏洞，请选择 **启用静态应用程序安全测试 (SAST)**。
1. 选择 **创建项目**。

## 从内置模板创建项目

<!--
A built-in project template populates a new project with files to get you started.
Built-in templates are sourced from the following groups:

- [`project-templates`](https://gitlab.com/gitlab-org/project-templates)
- [`pages`](https://gitlab.com/pages)

Anyone can contribute a built-in template by following [these steps](https://about.gitlab.com/community/contribute/project-templates/).
-->

要从内置模板创建项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **创建新项目**。
1. 选择 **从模板创建**。
1. 选择 **内置** 选项卡。
1. 从模板列表中：
   - 要查看模板的预览，请选择 **预览**。
   - 要为项目使用模板，请选择 **使用模板**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。您可以使用空格、连字符、下划线和表情符号。不能使用特殊字符。输入名称后，将填入 **项目标识串**。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 要创建 README 文件以便初始化 Git 仓库、具有默认分支并可以克隆，请选择 **使用自述文件初始化仓库**。
   - 要分析项目中的源代码是否存在已知安全漏洞，请选择 **启用静态应用程序安全测试 (SAST)**。
1. 选择 **创建项目**。

## 从自定义模板创建项目 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/6860) in [GitLab Premium](https://about.gitlab.com/pricing/) 11.2.
-->

自定义项目模板可在以下位置获得：

<!--
- The [instance-level](../../user/admin_area/custom_project_templates.md)
- The [group-level](../../user/group/custom_project_templates.md)
-->

- 实例级别
- 群组级别

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **创建新项目**。
1. 选择 **从模板创建**。
1. 选择 **实例** 或 **群组** 选项卡。
1. 从模板列表中：
   - 要查看模板的预览，请选择 **预览**。
   - 要为项目使用模板，请选择 **使用模板**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。您可以使用空格、连字符、下划线和表情符号。不能使用特殊字符。输入名称后，将填入 **项目标识串**。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 要创建 README 文件以便初始化 Git 仓库、具有默认分支并可以克隆，请选择 **使用自述文件初始化仓库**。
   - 要分析项目中的源代码是否存在已知安全漏洞，请选择 **启用静态应用程序安全测试 (SAST)**。
1. 选择 **创建项目**。

## 从 HIPAA 审计协议模板创建项目 **(ULTIMATE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13756) in GitLab 12.10
-->

HIPAA 审计协议模板包含美国卫生与公共服务部发布的 HIPAA 审计协议中的审计查询问题。

从 HIPAA 审计协议模板创建项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **创建新项目**。
1. 选择 **从模板创建**。
1. 选择 **内置**选项卡。
1. 找到 **HIPAA 审计协议** 模板：
   - 要查看模板的预览，请选择 **预览**。
   - 要为项目使用模板，请选择 **使用模板**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。您可以使用空格、连字符、下划线和表情符号。不能使用特殊字符。输入名称后，将填入 **项目标识串**。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 要创建 README 文件以便初始化 Git 仓库、具有默认分支并可以克隆，请选择 **使用自述文件初始化仓库**。
   - 要分析项目中的源代码是否存在已知安全漏洞，请选择 **启用静态应用程序安全测试 (SAST)**。
1. 选择 **创建项目**。

## 推送创建一个新项目

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/26388) in GitLab 10.5.
-->

使用 `git push` 将本地项目仓库推送到极狐GitLab。推送仓库后，极狐GitLab 在您选择的命名空间中创建您的项目。

您不能使用 `git push` 来创建具有以下项目路径的项目：

- 以前使用过。
- 已[重命名](settings/index.md#重命名仓库)。

以前使用的项目路径有一个重定向，重定向会导致推送尝试将请求重定向到重命名的项目位置，而不是创建新项目。要为以前使用过或重命名的项目创建新项目，请使用 [UI](#创建项目) 或项目 API<!--[项目 API](../../api/projects.md#create-project)-->。

先决条件

- 要使用 SSH 推送，您必须有[一个 SSH 密钥](../../ssh/index.md)，添加到您的极狐GitLab 帐户<!--[添加到您的极狐GitLab 帐户](../../ssh/index.md#add-an-ssh-key-to-your-gitlab-account)-->。
- 您必须具有向命名空间添加新项目的权限。要检查您是否有权限：

   1. 在顶部栏上，选择 **菜单 > 项目**。
   1. 选择 **群组**。
   1. 选择一个群组。
   1. 确认右上角有 **新建项目**。如果您需要权限，请联系您的 GitLab 管理员。

要推送您的仓库并创建一个项目：

1. 使用 SSH 或 HTTPS 推送：
   - 使用 SSH 推送：

      ```shell
      git push --set-upstream git@gitlab.example.com:namespace/myproject.git master
      ```

   - 使用 HTTPS 推送：

      ```shell
      git push --set-upstream https://gitlab.example.com/namespace/myproject.git master
      ```

    - 对于 `gitlab.example.com`，使用托管 Git 仓库的机器的域名。
    - 对于 `namespace`，使用您的[命名空间](../group/index.md#命名空间)的名称。
    - 对于 `myproject`，使用你的项目名称。
    - 可选。要导出现有的仓库标签，请将 `--tags` 标志附加到您的 `git push` 命令。
1. 可选。 要配置远端：
   ```shell
   git remote add origin https://gitlab.example.com/namespace/myproject.git
   ```

推送完成后，系统会显示以下消息：

```shell
remote: The private project namespace/myproject was created.
```

要查看您的新项目，请转到 `https://gitlab.example.com/namespace/myproject`。
默认情况下，您项目的可见性设置为 **私有**。要更改项目可见性，请调整您的项目设置<!--[项目设置](../../public_access/public_access.md#change-project-visibility)-->。

## 星标一个项目

您可以为经常使用的项目添加星号，以便更容易找到它们。

给项目加星：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **您的项目** 或 **浏览项目**。
1. 选择一个项目。
1. 在页面右上角，选择 **星标**。

## 查看星标项目

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **星标项目**。
1. 系统显示有关您已加星标的项目的信息，包括：

    - 项目描述，包括名称、描述和图标。
    - 该项目被星标的次数。
    - 该项目被派生的次数。
    - 打开合并请求的数量。
    - 开放议题的数量。

## 删除项目

删除项目后，个人命名空间中的项目会立即删除。要延迟删除群组中的项目，您可以[启用延迟项目删除](../group/index.md#启用延迟项目移除)。

要删除项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **您的项目** 或 **浏览项目**。
1. 选择一个项目。
1. 选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 向下滚动到 **删除项目** 部分。
1. 选择 **删除项目**。
1. 通过填写字段确认此操作。

## 查看项目动态

要查看项目的动态：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **您的项目** 或 **浏览项目**。
1. 选择一个项目。
1. 在左侧边栏上，选择 **项目信息 > 动态**。
1. 选择一个选项卡以查看项目动态的类型。

## 离开一个项目

如果您离开项目，您将不再是项目成员并且无法做出贡献。

离开项目：

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **您的项目** 或 **浏览项目**。
1. 选择一个项目。
1. 选择 **离开项目**。**离开项目** 选项仅在项目属于[群组命名空间](../group/index.md#namespaces)下的群组时，才会显示在项目仪表板上。

## 使用项目作为 Go 包

先决条件：

- 联系您的管理员以启用 GitLab Go Proxy<!--[GitLab Go Proxy](../packages/go_proxy/index.md)-->。
- 要将子组中的私有项目用作 Go 包，您必须[验证 Go 请求](#验证对私有项目的-go-请求)。未经身份验证的 Go 请求会导致 `go get` 失败。 对于不在子组中的项目，您不需要验证 Go 请求。

要将项目用作 Go 包，请使用 `go get` 和 `godoc.org` 发现请求。您可以使用元标签：

- [`go-import`](https://pkg.go.dev/cmd/go#hdr-Remote_import_paths)
- [`go-source`](https://github.com/golang/gddo/wiki/Source-Code-Links)

### 验证对私有项目的 Go 请求

先决条件：

- 您的极狐GitLab 实例必须可通过 HTTPS 访问。
- 您必须拥有 [个人访问令牌](../profile/personal_access_tokens.md)。

要验证 Go 请求，请创建一个 [`.netrc`](https://everything.curl.dev/usingcurl/netrc) 文件，其中包含以下信息：

```plaintext
machine gitlab.example.com
login <gitlab_user_name>
password <personal_access_token>
```

在 Windows 上，Go 读取 `~/_netrc` 而不是 `~/.netrc`。

`go` 命令不会通过不安全的连接传输凭据。它验证 Go 发出的 HTTPS 请求，但不验证通过 Git 发出的请求。

### 验证 Git 请求

如果 Go 无法从代理获取模块，它会使用 Git。Git 使用 `.netrc` 文件对请求进行身份验证，但您可以配置其他身份验证方法。

将 Git 配置为：

- 在请求 URL 中嵌入凭据：

    ```shell
    git config --global url."https://${user}:${personal_access_token}@gitlab.example.com".insteadOf "https://gitlab.example.com"
    ```

- 使用 SSH 而不是 HTTPS：

    ```shell
    git config --global url."git@gitlab.example.com".insteadOf "https://gitlab.example.com"
    ```

### 禁用私有项目的 Go 模块获取

为了获取模块或包，Go 使用环境变量：

- `GOPRIVATE`
- `GONOPROXY`
- `GONOSUMDB`

要禁用获取：

1. 禁用 `GOPRIVATE`：
     - 要禁用对一个项目的查询，请禁用 `GOPRIVATE=gitlab.example.com/my/private/project`。
     - 要禁用 SaaS 上所有项目的查询，请禁用 `GOPRIVATE=gitlab.example.com`。
1. 在 `GONOPROXY` 中禁用代理查询。
1. 禁用 `GONOSUMDB` 中的校验和查询。
- 如果模块名称或其前缀在 `GOPRIVATE` 或 `GONOPROXY` 中，Go 不会查询模块代理。
- 如果模块名称或其前缀在 `GONOPRIVATE` 或 `GONOSUMDB` 中，Go 不会查询校验和数据库。

### 从 Geo 次要站点获取 Go 模块

使用 Geo<!--[Geo](../../administration/geo/index.md)--> 访问包含次要 Geo 服务器上的 Go 模块的 Git 存储库。

您可以使用 SSH 或 HTTP 访问 Geo 次要服务器。

#### 使用 SSH 访问 Geo 次要服务器

要使用 SSH 访问 Geo 次要服务器：

1. 在客户端重新配置 Git 以将主服务器的流量发送到次要服务器：

   ```shell
   git config --global url."git@gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
   git config --global url."git@gitlab-secondary.example.com".insteadOf "http://gitlab.example.com"
   ```

    - For `gitlab.example.com`, use the primary site domain name.
    - For `gitlab-secondary.example.com`, use the secondary site domain name.

1. 确保客户端设置为通过 SSH 访问 GitLab 仓库。您可以在主服务器上进行测试，系统会将公钥复制到次要服务器。

`go get` 请求生成到主 Geo 服务器的 HTTP 流量。当模块下载开始时，`insteadOf` 配置将流量发送到次要 Geo 服务器。

#### 使用 HTTP 访问 Geo 次要节点

您必须使用复制到次要服务器的持久访问令牌。您不能使用 CI/CD 作业令牌通过 HTTP 获取 Go 模块。

使用 HTTP 访问 Geo 次要服务器：

1. 在客户端添加一个 Git `insteadOf` 重定向：

   ```shell
   git config --global url."https://gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
   ```

   - 对于 `gitlab.example.com`，使用主站点域名。
   - 对于 `gitlab-secondary.example.com`，使用次要站点域名。

1. 生成[个人访问令牌](../profile/personal_access_tokens.md)并在客户端的`~/.netrc` 文件中添加凭据：

   ```shell
   machine gitlab.example.com login USERNAME password TOKEN
   machine gitlab-secondary.example.com login USERNAME password TOKEN
   ```

`go get` 请求生成到主 Geo 服务器的 HTTP 流量。当模块下载开始时，`insteadOf` 配置将流量发送到次要 Geo 服务器。

<!--
## Related topics

- [Import a project](../../user/project/import/index.md).
- [Connect an external repository to GitLab CI/CD](../../ci/ci_cd_for_external_repos/index.md).
- [Fork a project](repository/forking_workflow.md#creating-a-fork).
- [Adjust project visibility and access levels](settings/index.md#sharing-and-permissions).
-->
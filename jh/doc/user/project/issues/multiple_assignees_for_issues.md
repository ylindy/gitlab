---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 议题的多指派人（专业版） **(PREMIUM)**

> - 在 13.9 版本中移到专业版

在大型团队中，存在对一个议题的所有权问题，可能很难跟踪谁在处理它，谁已经完成了他们的提交，谁甚至还没有开始。

您还可以为一个议题选择多个[指派人](managing_issues.md#指派人)，这样更容易跟踪，并明确谁对它负责。

![议题的多指派人](img/multiple_assignees_for_issues.png)

## 用例

考虑一个由前端开发人员、后端开发人员组成的团队，UX 设计师、QA 测试人员和产品经理共同努力将创意推向市场。

议题的多个指派人使协作更顺畅，可以清楚地显示共享的责任。 所有指派人都会显示在您团队的工作流程中并接收通知（就像他们作为单个指派人一样），从而简化了沟通成本和所有权混乱的问题。

一旦指派人完成了他们的工作，他们就会将自己作为指派人移除，明确表示他们的角色已经完成。

## 如何工作

在一个打开的议题中，展开右侧边栏，找到指派人条目，然后单击**编辑**。从下拉菜单中，选择您要将问题分配给的用户。

![增加多个指派人](img/multiple_assignees.gif)

移除指派人，只需从下拉框中将其取消选择即可。 

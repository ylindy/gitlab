---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目集成 **(FREE)**

您可以在项目的 **设置 > 集成** 页面下找到可用的集成。您至少需要在项目上拥有维护者角色<!--[维护者角色](../../permissions.md)-->。

## 集成

与插件一样，集成允许您将 GitLab 与其他应用程序集成，添加附加功能。<!--有关更多信息，请阅读[集成概述](overview.md) 或了解如何管理您的集成：
-->

<!--
- *对于 13.3 及更高版本，* 阅读 [项目集成管理](../../admin_area/settings/project_integration_management.md)。

- *For GitLab 13.2 and earlier,* read [Service Templates](services_templates.md),
  which are deprecated and [scheduled to be removed](https://gitlab.com/gitlab-org/gitlab/-/issues/268032)
  in GitLab 14.0.
-->

## 项目 webhooks

项目 webhook 允许您在推送新代码或创建新问题时触发 URL。您可以配置 webhook 来侦听特定事件，例如推送、议题或合并请求。GitLab 将带有数据的 POST 请求发送到 webhook URL。

<!--了解更多[关于 webhooks](webhooks.md)。-->
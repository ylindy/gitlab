---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 分支 **(FREE)**

分支是项目工作树的一个版本。您为所做的每组相关的更改创建一个分支。这使每组更改彼此分开，允许并行进行更改，而不会相互影响。

将更改推送到新分支后，您可以：

- 创建一个[合并请求](../../merge_requests/index.md)
- 执行内联代码审查
- [讨论](../../../discussions/index.md)您和您的团队的实施
- 使用 [Review Apps](../../../../ci/review_apps/index.md) 预览提交到新分支的更改。

您还可以向您的经理请求[批准](../../merge_requests/approvals/index.md)。

有关使用 GitLab UI 管理分支的更多信息，请参阅：

- [默认分支](default.md)：当您创建一个新的[项目](../../index.md) 时，系统会为仓库创建一个默认分支。您可以在项目、子组、群组或实例级别更改此设置。
- 创建分支<!--[创建分支](../web_editor.md#create-a-new-branch)-->
- 受保护的分支<!--[受保护的分支](../../protected_branches.md#protected-branches)-->
- [删除合并的分支](#删除合并的分支)
- [分支过滤器搜索框](#分支过滤器搜索框)

您还可以使用[命令行](../../../../gitlab-basics/start-using-git.md#创建一个分支)管理分支。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>Watch the video [GitLab Flow](https://www.youtube.com/watch?v=InKNIvky2KE).

See also:

- [Branches API](../../../../api/branches.md), for information on operating on repository branches using the GitLab API.
- [GitLab Flow](../../../../topics/gitlab_flow.md) documentation.
- [Getting started with Git](../../../../topics/git/index.md) and GitLab.
-->

## 比较

要比较仓库中的分支：

1. 导航到您的项目的仓库。
1. 在侧边栏中选择 **仓库 > 比较**。
1. 选择目标仓库与[仓库过滤搜索框](#仓库过滤搜索框)进行对比。
1. 使用[分支过滤器搜索框](#分支过滤器搜索框)选择要比较的分支。
1. 点击 **比较** 内联查看更改：

   ![compare branches](img/compare_branches_v13_12.png)

## 删除合并的分支

![Delete merged branches](img/delete_merged_branches.png)

此功能允许批量删除合并的分支。作为此操作的一部分，仅删除已合并且不受保护<!--[不受保护](../../protected_branches.md)-->的分支。

清理合并请求时未自动删除的旧分支特别有用。

## 仓库过滤搜索框

> 引入于 13.10 版本。

此功能允许您在[比较分支](#比较)时快速搜索和选择仓库。

![Repository filter search box](img/repository_filter_search_box_v13_12.png)

搜索结果按以下顺序显示：

- 名称与搜索词完全匹配的仓库。
- 名称包含搜索词的其他仓库，按字母顺序排序。

## 分支过滤器搜索框

![Branch filter search box](img/branch_filter_search_box_v13_12.png)

此功能允许您快速搜索和选择分支。搜索结果按以下顺序显示：

- 名称与搜索词完全匹配的分支。
- 名称包含搜索词的其他分支，按字母顺序排序。

有时，当您有数百个分支时，您可能需要更灵活的匹配模式。 在这种情况下，您可以使用以下方法：

- `^feature` 只匹配以 'feature' 开头的分支名称。
- `feature$` 只匹配以 'feature' 结尾的分支名称。

## 交换修订

> 引入于 13.12 版本。

![Before swap revisions](img/swap_revisions_before_v13_12.png)

交换修订功能允许您交换源和目标修订。单击“交换修订”按钮时，将交换源和目标的选定修订。

![After swap revisions](img/swap_revisions_after_v13_12.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

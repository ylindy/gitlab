---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/repository_mirroring.html'
---

# 仓库镜像 **(FREE)**

您可以*镜像*外部源的仓库。您可以选择作为源的仓库，并修改仓库的哪些部分被复制。可以镜像分支、标签和提交。

存在几种镜像方法：

- [推送](push.md)：用于将极狐GitLab 仓库镜像到另一个位置。
- [拉取](pull.md)：用于将仓库从另一个位置镜像到极狐GitLab。
- [双向](bidirectional.md)镜像也可用，但会导致冲突。

在以下情况镜像仓库：

- 您的项目的规范版本已迁移到极狐GitLab。要在以前的家中继续提供项目的副本，请将您的极狐GitLab 仓库配置为[推送镜像](push.md)。您对极狐GitLab 仓库所做的更改将复制到旧位置。
- 您的极狐GitLab 项目是私有的，但某些组件可以公开共享。将您的主仓库配置为[推送镜像](push.md)，并推送您想要公开的部分。通过此配置，您可以开源特定项目，回馈开源社区，并保护项目的敏感部分。
- 您迁移到极狐GitLab，但您的项目的规范版本在其他地方。将您的极狐GitLab 仓库配置为另一个项目的[拉取镜像](pull.md)。您的极狐GitLab 仓库会提取项目的提交、标签和分支的副本。它们可以在极狐GitLab 上使用。

## 创建仓库镜像

先决条件：

- 您必须至少具有项目的维护者角色。
- 如果您的镜像使用 `ssh://` 连接，则主机密钥必须可在服务器上检测到，或者您必须拥有该密钥的本地副本。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **镜像仓库**。
1. 输入 **Git 仓库 URL**。出于安全原因，原始仓库的 URL 仅向镜像项目中具有维护者角色或所有者角色的用户展示。
1. 选择 **镜像方向**。
1. 如果您输入了 `ssh://` URL，请选择：
   - **检测主机密钥**：极狐GitLab 从服务器获取主机密钥并显示指纹。
   - **手动输入主机密钥**，并将主机密钥输入 **SSH 主机密钥**。

   在镜像仓库时，极狐GitLab 在连接之前确认至少一个存储的主机密钥匹配。此检查可以保护您的镜像免受恶意代码注入或您的密码被盗。

1. 选择 **验证方式**。<!--要了解更多信息，请阅读 [镜像的身份验证方法](#authentication-methods-for-mirrors)。-->
1. 如果您使用 SSH 主机密钥进行身份验证，请[验证主机密钥](#验证主机密钥)以确保其正确无误。
1. 为防止强制推送不同的 refs，请选择 [**保留分叉的 refs**](push.md#保留分叉的-refs)。
1. 可选。选择 [**仅镜像受保护的分支**](#仅镜像受保护的分支)。
1. 选择 **镜像仓库**。

如果您选择 `SSH 公钥` 作为您的身份验证方法，极狐GitLab 会为您的 GitLab 仓库生成一个公钥。您必须将此密钥提供给非 GitLab 服务器。
要了解更多信息，请阅读[获取您的 SSH 公钥](#获取您的-ssh-公钥)。

## 更新镜像

更新镜像仓库后，所有新分支、标签和提交都将在项目的活动源中可见。极狐GitLab 的仓库镜像会自动更新。
您还可以手动触发更新：

<!--
- 在 GitLab.com 上最多每五分钟一次。
-->

- 根据管理员在自助管理实例上设置的拉取镜像间隔限制<!--[拉镜像间隔限制](../../../../administration/instance_limits.md#pull-mirroring-interval)-->。

### 强制更新

虽然镜像计划自动更新，但您可以强制立即更新，除非：

- 镜像已经在更新。
- 拉取镜像限制的间隔（以秒为单位）<!--[间隔（以秒为单位）](../../../../administration/instance_limits.md#pull-mirroring-interval)-->在上次更新后尚未结束。

先决条件：

- 您必须至少具有项目的维护者角色。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **镜像仓库**。
1. 滚动到 **镜像的仓库** 并确定要更新的镜像。
1. 选择 **立即更新**（**{retry}**）：
   ![Repository mirroring force update user interface](img/repository_mirroring_force_update.png)

## 仅镜像受保护的分支 **(PREMIUM)**

<!--
> Moved to GitLab Premium in 13.9.
-->

您可以选择仅从或向远端镜像镜像项目中的 [受保护分支](../../protected_branches.md)。 对于[pull mirroring](pull.md)，镜像项目中的非保护分支不进行镜像，可以发散。

您可以选择仅将镜像项目中的[受保护分支](../../protected_branches.md)，镜像到远端仓库或从远端仓库镜像。 对于[拉取镜像](pull.md)，镜像项目中的非保护分支不进行镜像，可以分叉。

要使用此选项，请在创建仓库镜像时选择 **仅镜像受保护的分支**。

## 镜像的验证方法

创建镜像时，必须为其配置身份验证方法。
极狐GitLab 支持以下身份验证方法：

- [SSH 认证](#ssh-验证)。
- 密码。

### SSH 验证

SSH 身份验证是相互的：

- 您必须向服务器证明您有权访问仓库。
- 服务器还必须*向您证明*它声明的是谁。

对于 SSH 身份验证，您提供您的凭据作为密码或*公钥*。
其它仓库所在的服务器提供其凭据作为*主机密钥*。
您必须手动[验证此主机密钥的指纹](#验证主机密钥)。

如果您通过 SSH 进行镜像（使用 `ssh://` URL），您可以使用以下方法进行身份验证：

- 基于密码的身份验证，就像通过 HTTPS 一样。
- 公钥验证。这种方法通常比密码验证更安全，尤其是当其它仓库支持部署密钥<!--[部署密钥](../../deploy_keys/index.md)-->时。

### 获取您的 SSH 公钥

当您镜像仓库并选择 **SSH 公钥** 作为您的身份验证方法时，极狐GitLab 会为您生成一个公钥。非 GitLab 服务器需要此密钥才能与您的 GitLab 仓库建立信任。要复制您的 SSH 公钥：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 存储库**。
1. 展开 **镜像仓库**。
1. 滚动到 **镜像的仓库**。
1. 确定正确的仓库，然后选择 **复制 SSH 公钥** (**{copy-to-clipboard}**)。
1. 将公共 SSH 密钥添加到另一个仓库的配置中：
   - 如果另一个仓库托管在极狐GitLab 上，请将公共 SSH 密钥添加为部署密钥<!--[部署密钥](../../../project/deploy_keys/index.md)-->。
   - 如果其它仓库托管在别处，请将密钥添加到您用户的 `authorized_keys` 文件中。将整个公共 SSH 密钥粘贴到文件中单独一行并保存。

如果您必须随时更改密钥，可以删除并重新添加镜像以生成新密钥。使用新密钥更新另一个仓库以保持镜像运行。

NOTE:
生成的密钥存储在 GitLab 数据库中，而不是文件系统中。因此，不能在预接收 hook 中使用镜像的 SSH 公钥认证。

### 验证主机密钥

使用主机密钥时，请始终验证指纹是否符合您的预期。

<!--
GitLab.com and other code hosting sites publish their fingerprints
for you to check:

- [AWS CodeCommit](https://docs.aws.amazon.com/codecommit/latest/userguide/regions.html#regions-fingerprints)
- [Bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/configure-ssh-and-two-step-verification/)
- [GitHub](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints)
- [GitLab.com](../../../gitlab_com/index.md#ssh-host-keys-fingerprints)
- [Launchpad](https://help.launchpad.net/SSHFingerprints)
- [Savannah](http://savannah.gnu.org/maintenance/SshAccess/)
- [SourceForge](https://sourceforge.net/p/forge/documentation/SSH%20Key%20Fingerprints/)
-->

<!--Other providers vary.--> 如果您符合以下条件，则可以使用以下命令安全地收集密钥指纹：

- 运行自助管理版极狐GitLab。
- 可以访问其他仓库的服务器。

```shell
$ cat /etc/ssh/ssh_host*pub | ssh-keygen -E md5 -l -f -
256 MD5:f4:28:9f:23:99:15:21:1b:bf:ed:1f:8e:a0:76:b2:9d root@example.com (ECDSA)
256 MD5:e6:eb:45:8a:3c:59:35:5f:e9:5b:80:12:be:7e:22:73 root@example.com (ED25519)
2048 MD5:3f:72:be:3d:62:03:5c:62:83:e8:6e:14:34:3a:85:1d root@example.com (RSA)
```

旧版本的 SSH 可能需要您从命令中删除 `-E md5`。

<!--
## Related topics

- Configure a [Pull Mirroring Interval](../../../../administration/instance_limits.md#pull-mirroring-interval)
- [Disable mirrors for a project](../../../admin_area/settings/visibility_and_access_controls.md#enable-project-mirroring)
- [Secrets file and mirroring](../../../../raketasks/backup_restore.md#when-the-secrets-file-is-lost)
-->

## 故障排查

如果在推送过程中发生错误，极狐GitLab 会为该仓库显示 **错误** 突出显示。然后可以通过将鼠标悬停在突出显示文本上来查看错误的详细信息。

### Received RST_STREAM with error code 2 with GitHub

如果您在镜像到 GitHub 仓库时收到此消息：

```plaintext
13:Received RST_STREAM with error code 2
```

您的 GitHub 设置可能设置为阻止公开您在提交中使用的电子邮件地址的推送。要解决此问题，请执行以下任一操作：

- 将您的 GitHub 电子邮件地址设置为公开。
- 禁用[阻止暴露我的电子邮件的命令行推送](https://github.com/settings/emails)设置。

### 超过最后期限

升级到 11.11.8 或更高版本时，用户名表示方式的更改意味着您必须更新镜像用户名和密码，以确保将 `%40` 字符替换为 `@`。

### Connection blocked because server only allows public key authentication

极狐GitLab 和远程存储库之间的连接被阻止。即使 <!--[TCP 检查](../../../../administration/raketasks/maintenance.md#check-tcp-connectivity-to-a-remote-site)-->TCP 检查成功，您必须检查从 GitLab 到远程服务器的路由中的任何网络组件是否被阻塞。

当防火墙对传出数据包执行“深度 SSH 检查”时，可能会发生此错误。

### Could not read username: terminal prompts disabled

如果在使用 GitLab CI/CD for external repositories<!--[GitLab CI/CD for external repositories](../../../../ci/ci_cd_for_external_repos/)--> 创建新项目后收到此错误：

```plaintext
"2:fetch remote: "fatal: could not read Username for 'https://bitbucket.org':
terminal prompts disabled\n": exit status 128."
```

检查镜像仓库的 URL 中是否指定了仓库所有者：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **镜像仓库**。
1. 如果没有指定仓库所有者，请删除并以这种格式再次添加 URL，将 `OWNER`、`ACCOUNTNAME` 和 `REPONAME` 替换为您的值：

   ```plaintext
   https://OWNER@bitbucket.org/ACCOUNTNAME/REPONAME.git
   ```

连接到仓库进行镜像时，Bitbucket 需要字符串中的仓库所有者。

### 拉取镜像缺少 LFS 文件

在某些情况下，拉取镜像不会传输 LFS 文件。在以下情况下会出现此问题：

- 您使用 SSH 存储库 URL。解决方法是改用 HTTPS 存储库 URL。
- 您使用的是 14.0 或更早版本，并且源存储库是公共 Bitbucket URL。在 14.0.6 版本中修复。
- 您使用对象存储来镜像外部仓库。

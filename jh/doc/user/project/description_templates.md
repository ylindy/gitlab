---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 描述模板 **(FREE)**

我们都知道，正确提交的议题更有可能被项目的开发人员及时解决。

使用描述模板，您可以为项目的议题和合并请求描述字段定义特定于上下文的模板，并从议题中过滤掉不必要的噪音。

通过使用描述模板，创建新议题或合并请求的用户可以选择描述模板来帮助他们与其他贡献者有效沟通。

被添加到项目仓库的根目录中的每个项目都可以定义自己的一组描述模板。

描述模板必须使用 Markdown<!--[Markdown](../markdown.md)-->，并存储在您的项目仓库中的 `.gitlab` 目录中。仅考虑默认分支的模板。

要了解如何为群组中的各种文件类型创建模板，请访问[群组文件模板](../group/index.md#群组文件模板)。

## 用例

在某些情况下，您可能会发现描述模板很有用：

- 您可以为工作流的不同阶段创建议题和合并请求模板，例如功能提案、功能改进或错误报告。
- 添加用于特定项目的每个议题的模板，提供说明和指南，需要特定于该主题的信息。例如，如果您有一个跟踪新博客帖子的项目，您可以要求标题、大纲、作者姓名和作者社交媒体信息。
- 按照前面的示例，您可以为每个随新博客文章提交的 MR 制作一个模板，需要有关发布日期、前文数据、图像指南、相关问题的链接、审稿人姓名等信息。
- 您还可以为工作流的不同阶段创建议题和合并请求模板，例如功能提案、功能改进或错误报告。
- 您可以使用[议题描述模板](#创建议题模板)作为[服务台电子邮件模板](service_desk.md#新服务台议题)。

## 创建议题模板

在仓库的 `.gitlab/issue_templates/` 目录中创建一个新的 Markdown (`.md`) 文件。提交并推送到您的默认分支。

创建 Markdown 文件：

1. 在一个项目中，进入 **仓库**。
1. 在默认分支旁边，选择 **{plus}** 按钮。
1. 选择 **新建文件**。
1. 在默认分支旁边的 **File name** 字段中，添加您的议题模板的名称。确保您的文件具有`.md` 扩展名，例如 `feature_request.md` 或`Feature Request.md`。
1. 提交并推送到您的默认分支。

如果您的仓库中没有 `.gitlab/issue_templates` 目录，则需要创建它。

创建 `.gitlab/issue_templates` 目录：

1. 在一个项目中，进入 **仓库**。
1. 在默认分支旁边，选择 **{plus}** 按钮。
1. 选择 **新建目录**。
1. 将这个新目录命名为 `.gitlab` 并提交到你的默认分支。
1. 在默认分支旁边，选择 **{plus}** 按钮。
1. 选择 **新建目录**。
1. 将您的目录命名为 `issue_templates` 并提交到您的默认分支。

要检查是否正常工作，请[创建新议题](issues/managing_issues.md#创建一个新议题)并查看您是否可以选择描述模板。

## 创建合并请求模板

与议题模板类似，在仓库的 `.gitlab/merge_request_templates/` 目录中创建一个新的 Markdown (`.md`) 文件。提交并推送到您的默认分支。

## 使用模板

假设您已经创建了文件 `.gitlab/issue_templates/Bug.md`，会在创建或编辑议题时启用 `Bug` 下拉选项。选择 `Bug` 时，`Bug.md` 模板文件中的内容将复制到议题描述字段。**重置模板** 按钮会放弃您在选择模板后所做的任何更改并将其返回到其初始状态。

NOTE:
您可以创建快捷链接以使用指定的模板创建问题。<!--例如：`https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal`。-->

![Description templates](img/description_templates.png)

您可以在不同级别设置描述模板：

- 整个[实例](#设置实例级描述模板)
- 指定的[群组或子组](#设置群组级描述模板)
- 指定的[项目](#为合并请求和议题设置默认模板)

模板是继承的。例如，在项目中，您还可以访问为实例或项目的父群组设置的模板。

### 设置实例级描述模板 **(PREMIUM SELF)**

> - 引入于 13.9 版本。
> - 功能标志移除于 14.0 版本。

您可以在 **实例级别** 为议题和合并请求设置描述模板。因此，这些模板在实例内的所有项目中都可用。

只有实例管理员可以设置实例级模板。

要设置实例级描述模板存储库：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 模板**。
1. 展开 **模板**。
1. 从下拉列表中，选择您的模板项目作为实例级别的模板仓库。
1. 选择 **保存更改**。

<!--
![Setting templates in the Admin Area](../admin_area/settings/img/file_template_admin_area_v14_0.png)

Learn more about [instance template repository](../admin_area/settings/instance_template_repository.md).
-->

### 设置群组级描述模板 **(PREMIUM)**

> - 引入于 13.9 版本。
> - 功能标志移除于 14.0 版本。

使用 **群组级别** 描述模板，您可以将模板存储在单个仓库中，并将群组文件模板设置配置为指向该仓库。因此，您可以在所有群组项目中的议题和合并请求中使用相同的模板。

要重用[您已创建](../project/description_templates.md#创建议题模板)的模板：

1. 进入群组的 **设置 > 通用 > 模板**。
1. 从下拉列表中，选择您的模板项目作为群组级别的模板仓库。
1. 选择 **保存更改**。

<!--
![Group template settings](../group/img/group_file_template_settings.png)
-->

### 为合并请求和议题设置默认模板 **(PREMIUM)**

在项目中，您可以为新议题和合并请求选择默认描述模板。因此，每次创建新的合并请求或议题时，都会预先填充您在模板中输入的文本。

议题或合并请求的可见性应在项目的 **设置/可见性, 项目功能, 权限** 部分中设置为 “每个人都有访问权限” 或 “仅限项目成员”。否则，模板文本区域不会显示。这是默认行为，因此在大多数情况下您应该没问题。

为合并请求设置默认描述模板：

1. 转到您项目的 **设置**。
1. 在 **合并请求** 标题下选择 **展开**。
1. 填写 **合并请求的默认描述模板** 文本区域。
1. 选择 **保存更改**。

要为议题设置默认描述模板：

1. 选择 **默认议题模板**下的 **展开**。
1. 填写 **议题的默认描述模板** 文本区域。

由于合并请求和议题支持 Markdown<!--[Markdown](../markdown.md)-->，您可以使用它来格式化标题、列表等。

13.10 及更高版本在项目 API <!--[项目 API](../../api/projects.md)-->以帮助您使模板保持最新。

## 描述模板示例

我们在项目的 [`.gitlab` 文件夹](https://gitlab.com/gitlab-jh/gitlab/-/tree/master/.gitlab) 中使用议题和合并请求的描述模板，您可以参考一些例子。

NOTE:
可以在描述模板中使用[快速操作](quick_actions.md)来快速添加标记、指派人和里程碑。仅当提交议题或合并请求的用户具有执行相关操作的权限时，才会执行快速操作。

以下是错误报告模板的示例：

```markdown
## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Example Project

(If possible, please create an example project here on GitLab.com that exhibits the problematic
behavior, and link to it here in the bug report.
If you are using an older version of GitLab, this will also determine whether the bug has been fixed
in a more recent version)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug ~reproduced ~needs-investigation
/cc @project-manager
/assign @qa-tester
```

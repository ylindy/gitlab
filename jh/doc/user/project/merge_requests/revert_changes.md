---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 还原更改 **(FREE)**

您可以使用 Git 的强大功能来[还原任何提交](https://git-scm.com/docs/git-revert "Git 还原文档")，方法是单击合并请求和提交详细信息中的 **还原** 按钮。

## 还原合并请求

NOTE:
**还原** 按钮仅适用于 8.5 及更高版本中创建的合并请求。但是，您仍然可以通过从提交页面列表中恢复合并提交来恢复合并请求。

NOTE:
**还原** 按钮仅对使用合并方法 “Merge Commit” 的项目显示，可以在项目的 **设置 > 通用 > 合并请求** 下设置。[快进提交](fast_forward_merge.md) 不能通过使用合并请求视图来恢复。

合并请求合并后，使用 **还原** 按钮还原该合并请求引入的更改。

![Revert merge request](img/cherry_pick_changes_mr.png)

单击该按钮后，会出现一个窗口，您可以在其中选择将更改直接还原到所选分支，或者您可以选择使用还原更改创建新的合并请求。

合并请求被还原后，**还原** 按钮不再可用。

## 还原提交

您可以从提交详细信息页面还原提交：

![Revert commit](img/cherry_pick_changes_commit.png)

与还原合并请求类似，您可以选择将更改直接还原到目标分支或创建新的合并请求以还原更改。

还原提交后，**还原** 按钮不再可用。

还原合并提交时，主线始终是第一个父级。如果要使用不同的主线，则需要从命令行执行此操作。

这是使用第二个父级作为主线恢复合并提交的示例：

```shell
git revert -m 2 7a39eb0
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

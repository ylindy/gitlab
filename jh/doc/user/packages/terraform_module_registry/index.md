---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Terraform module 库 **(FREE)**

> 引入于 14.0 版本。

在项目的基础设施库中发布 Terraform module，然后使用极狐GitLab 作为 Terraform module 库引用它们。

## Terraform module 库身份验证

要对 Terraform module 库进行身份验证，您需要：

- 至少具有 `read_api` 权限的个人访问令牌<!--[个人访问令牌](../../../api/index.md#personalproject-access-tokens)-->。
- CI/CD 作业令牌<!--[CI/CD 作业令牌](../../../ci/jobs/ci_job_token.md)-->。

## 发布 Terraform Module

当您发布 Terraform Module 时，如果它不存在，则会创建它。

如果已存在具有相同名称和版本的包，则不会创建该包。它不会覆盖现有的包。

先决条件：

- 您需要使用 API 进行身份验证<!--[使用 API 进行身份验证](../../../api/index.md#authentication)-->。如果使用部署令牌进行身份验证，则必须使用 `write_package_registry` 范围进行配置。

```plaintext
PUT /projects/:id/packages/terraform/modules/:module-name/:module-system/:module-version/file
```

| 属性          | 类型           | 是否必需 | 描述                                                                                                                      |
| -------------------| --------------- | ---------| -------------------------------------------------------------------------------------------------------------------------------- |
| `id`               | integer/string  | yes      | ID 或项目的 URL 编码路径<!--[项目的 URL 编码路径](../../../api/index.md#namespaced-path-encoding)-->。                                    |
| `module-name`      | string          | yes      | 包名称。它只能包含小写字母 (`a-z`)、大写字母 (`A-Z`)、数字 (`0-9`) 或连字符 (`-`)，并且不能超过 64 个字符。
| `module-system`    | string          | yes      | 包系统。它只能包含小写字母（`a-z`）和数字（`0-9`），并且不能超过 64 个字符。
| `module-version`   | string          | yes      | 包版本。根据[语义版本规范](https://semver.org/)，它必须是有效的。

在请求正文中提供文件内容。

请注意，在以下示例中，请求必须以 `/file` 结尾。
如果您发送以其他内容结尾的请求，则会导致 404 错误 `{"error":"404 Not Found"}`。

使用个人访问令牌的示例请求：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     --upload-file path/to/file.tgz \
     "https://gitlab.example.com/api/v4/projects/<your_project_id>/packages/terraform/modules/my-module/my-system/0.0.1/file"
```

示例响应：

```json
{
  "message":"201 Created"
}
```

使用部署令牌的示例请求：

```shell
curl --header "DEPLOY-TOKEN: <deploy_token>" \
     --upload-file path/to/file.tgz \
     "https://gitlab.example.com/api/v4/projects/<your_project_id>/packages/terraform/modules/my-module/my-system/0.0.1/file"
```

示例响应：

```json
{
  "message":"201 Created"
}
```

## 引用 Terraform Module

先决条件：

- 您需要使用 API 进行身份验证<!--[使用 API 进行身份验证](../../../api/index.md#authentication)-->。如果使用个人访问令牌进行身份验证，则必须使用 `read_api` 范围进行配置。

可以在您的 `~/.terraformrc` 文件中为 `terraform` 提供身份验证令牌（作业令牌或个人访问令牌）：

```plaintext
credentials "gitlab.cn" {
  token = "<TOKEN>"
}
```

其中 `gitlab.cn` 可以替换为您的自助管理实例的主机名。

然后，您可以从下游 Terraform 项目中引用您的 Terraform module：

```plaintext
module "<module>" {
  source = "gitlab.com/<namespace>/<module-name>/<module-system>"
}
```

## 使用 CI/CD 发布 Terraform module

要使用 [GitLab CI/CD](../../../ci/index.md) 中的 Terraform module，您可以使用 `CI_JOB_TOKEN` 代替命令中的个人访问令牌。

例如：

```yaml
image: curlimages/curl:latest

stages:
  - upload

upload:
  stage: upload
  script:
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file path/to/file.tgz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/my-module/my-system/0.0.1/file"'
```

<!--
## Example projects

For examples of the Terraform module registry, check the projects below:

- The [_GitLab local file_ project](https://gitlab.com/mattkasa/gitlab-local-file) creates a minimal Terraform module and uploads it into the Terraform module registry using GitLab CI/CD.
- The [_Terraform module test_ project](https://gitlab.com/mattkasa/terraform-module-test) uses the module from the previous example.
-->
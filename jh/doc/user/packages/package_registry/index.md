---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 软件包库 **(FREE)**

<!--
> [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/221259) from GitLab Premium to GitLab Free in 13.3.
-->

使用 GitLab 软件包库，您可以将极狐GitLab 用作各种[支持的包管理器](#支持的软件包管理器)的私有或公开库。
您可以发布和共享包，这些包可以作为下游项目的依赖项使用。

## 查看软件包

您可以查看项目或群组的软件包。

1. 进入项目或群组。
1. 进入 **软件包和镜像库 > 软件包库**。

您可以在此页面上搜索、排序和过滤包。您可以通过从浏览器复制和粘贴 URL 来共享您的搜索结果。

您还可以找到用于配置包管理器或安装特定软件包的有用代码片段。

当您查看群组中的软件包时：

- 显示发布到群组的所有项目。
- 仅显示您可以访问的项目。
- 如果项目是私有的，或者您不是该项目的成员，则不会显示。

<!--
有关如何创建和上传软件包的信息，请查看您的包类型的 GitLab 文档。
-->

## 使用 GitLab CI/CD 构建包

您可以使用 [GitLab CI/CD](../../../ci/index.md) 来构建包。
对于 Maven、NuGet、npm、Conan、Helm 和 PyPI 包以及 Composer 依赖项，您可以使用 `CI_JOB_TOKEN` 向极狐GitLab 进行身份验证。

您可以用来开始的 CI/CD 模板位于[此仓库](https://gitlab.com/gitlab-jh/gitlab/-/tree/master/lib/gitlab/ci/templates)中。

<!--
了解有关将 GitLab 软件包库与 CI/CD 结合使用的更多信息：

- [Composer](../composer_repository/index.md#publish-a-composer-package-by-using-cicd)
- [Conan](../conan_repository/index.md#publish-a-conan-package-by-using-cicd)
- [Generic](../generic_packages/index.md#publish-a-generic-package-by-using-cicd)
- [Maven](../maven_repository/index.md#create-maven-packages-with-gitlab-cicd)
- [npm](../npm_registry/index.md#publish-an-npm-package-by-using-cicd)
- [NuGet](../nuget_repository/index.md#publish-a-nuget-package-by-using-cicd)
- [PyPI](../pypi_repository/index.md#authenticate-with-a-ci-job-token)
- [RubyGems](../rubygems_registry/index.md#authenticate-with-a-ci-job-token)
-->

如果使用 CI/CD 构建包，查看包详情时会显示扩展活动信息：

![Package CI/CD activity](img/package_activity_v12_10.png)

您可以查看发布包的流水线，以及触发它的提交和用户。但是，历史记录仅限于给定包的五次更新。

## 下载包

要下载软件包：

1. 进入 **软件包和镜像库 > 软件包库**。
1. 选择要下载的包的名称。
1. 在 **活动** 部分，选择您要下载的包的名称。

## 删除包

在软件宝库中发布包后，您将无法对其进行编辑。相反，您必须删除并重新创建它。

要删除包，您必须具有合适的权限。

您可以使用 API 或 UI 删除包。

要在 UI 中从您的组或项目中删除包：

1. 进入 **软件包和镜像库 > 软件包库**。
1. 找到要删除的包的名称。
1. 点击 **删除**。

包被永久删除。

## 删除与包关联的文件

要删除包文件，您必须具有合适的权限。

您可以使用 API 或 UI 删除包。

要从您的群组或项目中删除 UI 中的包文件：

1. 进入 **软件包和镜像库 > 软件包库**。
1. 找到要删除的包的名称。
1. 选择包以查看其他详细信息。
1. 找到您要删除的文件的名称。
1. 展开省略号并选择 **删除文件**。

包文件被永久删除。

## 禁用软件包库

软件包库自动启用。

如果您使用的是自助管理实例，您的管理员可以从侧栏中删除菜单项 **软件包和镜像库**。<!--有关详细信息，请参阅 [管理文档](../../../administration/packages/index.md)。-->

您还可以专门为您的项目删除软件包库：

1. 在您的项目中，转到 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限** 部分并禁用 **软件包** 功能。
1. 单击 **保存修改**。

**软件包和镜像库 > 软件包库** 条目从侧边栏中删除。

<!--
## 软件包工作流

Learn how to use the GitLab Package Registry to build your own custom package workflow:

- [Use a project as a package registry](../workflows/project_registry.md)
  to publish all of your packages to one project.

- Publish multiple different packages from one [monorepo project](../workflows/working_with_monorepos.md).
-->

## 支持的软件包管理器

WARNING:
并非所有包管理器格式都可以用于生产。要查看每种格式的状态，请参阅表格的 **状态** 列。

软件包库支持以下格式：

| 软件包类型 | GitLab 版本 | 状态 |
| ------------ | -------------- |------- |
| Maven | 11.3+ | GA |
| npm | 11.7+ | GA |
| NuGet | 12.8+ | GA |
| PyPI | 12.10+ | GA |
| Generic packages | 13.5+ | GA |
| Composer | 13.2+ | Beta |
| Conan | 12.6+ | Beta |
| Helm | 14.1+ | Beta |
| Debian | 14.2+ | Alpha |
| Go | 13.1+ | Alpha |
| Ruby gems | 13.10+ | Alpha |

状态说明：

- Alpha：在功能标志后面，不受官方支持。
- 测试版：可能会存在阻止使用的几个已知问题。
- GA（通用）：准备好以任何规模用于生产。

<!--
You can also use the [API](../../../api/packages.md) to administer the Package Registry.
-->

<!--
## Accepting contributions

This table lists unsupported package manager formats that we are accepting contributions for.
Consider contributing to GitLab. This [development documentation](../../../development/packages.md)
guides you through the process.
-->

<!-- vale gitlab.Spelling = NO -->

<!--
| Format | Status |
| ------ | ------ |
| Chef      | [#36889](https://gitlab.com/gitlab-org/gitlab/-/issues/36889) |
| CocoaPods | [#36890](https://gitlab.com/gitlab-org/gitlab/-/issues/36890) |
| Conda     | [#36891](https://gitlab.com/gitlab-org/gitlab/-/issues/36891) |
| CRAN      | [#36892](https://gitlab.com/gitlab-org/gitlab/-/issues/36892) |
| Opkg      | [#36894](https://gitlab.com/gitlab-org/gitlab/-/issues/36894) |
| P2        | [#36895](https://gitlab.com/gitlab-org/gitlab/-/issues/36895) |
| Puppet    | [#36897](https://gitlab.com/gitlab-org/gitlab/-/issues/36897) |
| RPM       | [#5932](https://gitlab.com/groups/gitlab-org/-/epics/5128)    |
| SBT       | [#36898](https://gitlab.com/gitlab-org/gitlab/-/issues/36898) |
| Swift     | [#12233](https://gitlab.com/gitlab-org/gitlab/-/issues/12233) |
| Vagrant   | [#36899](https://gitlab.com/gitlab-org/gitlab/-/issues/36899) |
-->

<!-- vale gitlab.Spelling = YES -->
